# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Accedian_skylight System. The API that was used to build the adapter for Accedian_skylight is usually available in the report directory of this adapter. The adapter utilizes the Accedian_skylight API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Accedian Skylight adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Accedian Skylight. With this adapter you have the ability to perform operations such as:

- Manage configuration and sensor orchestration service
- Get Endpoint
- Create Reflector Endpoint
- Perform Operation on Endpoint
- Get, Create, or Delete SLA

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
