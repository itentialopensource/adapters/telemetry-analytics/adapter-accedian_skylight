## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Accedian Skylight. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Accedian Skylight.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Accedian Skylight. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getEndpoints(callback)</td>
    <td style="padding:15px">Get endpoints</td>
    <td style="padding:15px">{base_path}/{version}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEndpoint(id, callback)</td>
    <td style="padding:15px">Delete endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alterEndpointDescription(id, description, callback)</td>
    <td style="padding:15px">Alter endpoint description.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}/description?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEndpointInformation(id, callback)</td>
    <td style="padding:15px">Get information about a endpoint (EndpointHead).</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}/endpointhead?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeNameOfEndpoint(id, name, callback)</td>
    <td style="padding:15px">Change name of endpoint.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">operationOnEndpoint(id, action, callback)</td>
    <td style="padding:15px">Operation on endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}/operate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInformationOnEndpointsByType(type, callback)</td>
    <td style="padding:15px">Get information on all endpoints of given type.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewReflectorEndpoint(body, callback)</td>
    <td style="padding:15px">Create a new unmanaged reflector endpoint.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/reflector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReflectorEndpointByName(id, callback)</td>
    <td style="padding:15px">Get reflector endpoint parameters by name (id).</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/reflector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeIpAddressUnmanagedEndpoint(id, ipAddress, callback)</td>
    <td style="padding:15px">Change ip address of terminated/unresolved unmanaged endpoint.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/reflector/{pathv1}/address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeCapabilityUnmanagedEndpoint(id, capabilities, callback)</td>
    <td style="padding:15px">Change capability of unmanaged endpoint.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/reflector/{pathv1}/capability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReflectorEndpointByMAC(id, callback)</td>
    <td style="padding:15px">Get reflector endpoint parameters by mac.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/reflector/{pathv1}/mac?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeTwampControlProtocolSettings(id, twampCp, callback)</td>
    <td style="padding:15px">Change Twamp control protocol settings of unmanaged endpoint.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/reflector/{pathv1}/twampcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTwampCp(id, callback)</td>
    <td style="padding:15px">Delete twamp cp.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/reflector/{pathv1}/twampcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSupervisonEndpoint(body, callback)</td>
    <td style="padding:15px">Create a new supervision endpoint.</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/supervision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSupervisionEndpointByName(id, callback)</td>
    <td style="padding:15px">Get supervision endpoint parameters by name (id).</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/supervision/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionInformation(callback)</td>
    <td style="padding:15px">Get information (SessionHead) on all nodes.</td>
    <td style="padding:15px">{base_path}/{version}/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSession(id, callback)</td>
    <td style="padding:15px">Delete a session.</td>
    <td style="padding:15px">{base_path}/{version}/session/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alterSessionDescription(id, description, callback)</td>
    <td style="padding:15px">Alter session description.</td>
    <td style="padding:15px">{base_path}/{version}/session/{pathv1}/description?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionHeadInformationByName(id, callback)</td>
    <td style="padding:15px">Get information (SessionHead) by name (id).</td>
    <td style="padding:15px">{base_path}/{version}/session/{pathv1}/head?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionLastRR(id, callback)</td>
    <td style="padding:15px">Get last RR.</td>
    <td style="padding:15px">{base_path}/{version}/session/{pathv1}/lastrr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alterTerminatedNotResolvedSessionName(id, body, callback)</td>
    <td style="padding:15px">Alter a Terminated or NotResolved session name.</td>
    <td style="padding:15px">{base_path}/{version}/session/{pathv1}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">operationOnSession(id, action, callback)</td>
    <td style="padding:15px">Operation on session.</td>
    <td style="padding:15px">{base_path}/{version}/session/{pathv1}/operate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alterTerminatedNotResolvedSessionStream(id, stream, callback)</td>
    <td style="padding:15px">Alter Terminated or NotResolved session stream.</td>
    <td style="padding:15px">{base_path}/{version}/session/{pathv1}/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEchoSession(body, callback)</td>
    <td style="padding:15px">Create a new ECHO session.</td>
    <td style="padding:15px">{base_path}/{version}/session/echo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEchoSessionInformation(id, callback)</td>
    <td style="padding:15px">Get information about a ECHO session.</td>
    <td style="padding:15px">{base_path}/{version}/session/echo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEthOamSession(body, callback)</td>
    <td style="padding:15px">Create a new eth-oam session.</td>
    <td style="padding:15px">{base_path}/{version}/session/ethoam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEthOamSessionInformation(id, callback)</td>
    <td style="padding:15px">Get information about a eth-oam session.</td>
    <td style="padding:15px">{base_path}/{version}/session/ethoam/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTWAMPSession(body, callback)</td>
    <td style="padding:15px">Create a new TWAMP session.</td>
    <td style="padding:15px">{base_path}/{version}/session/twamp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTWAMPSessionInformation(id, callback)</td>
    <td style="padding:15px">Get information about a TWAMP session.</td>
    <td style="padding:15px">{base_path}/{version}/session/twamp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSLA(body, callback)</td>
    <td style="padding:15px">Create a new SLA.</td>
    <td style="padding:15px">{base_path}/{version}/sla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSLAs(callback)</td>
    <td style="padding:15px">Get all SLAs</td>
    <td style="padding:15px">{base_path}/{version}/sla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSLA(id, callback)</td>
    <td style="padding:15px">Delete a SLA</td>
    <td style="padding:15px">{base_path}/{version}/sla/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSessionReferenceToSLA(id, sessionRef, callback)</td>
    <td style="padding:15px">Put session reference to SLA.</td>
    <td style="padding:15px">{base_path}/{version}/sla/{pathv1}/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionReferencesForSLA(id, callback)</td>
    <td style="padding:15px">Get all session references for sla.</td>
    <td style="padding:15px">{base_path}/{version}/sla/{pathv1}/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSessionReferenceFromSLA(id, ref, callback)</td>
    <td style="padding:15px">Delete a session reference from a sla.</td>
    <td style="padding:15px">{base_path}/{version}/sla/{pathv1}/session/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemVersion(callback)</td>
    <td style="padding:15px">System Version</td>
    <td style="padding:15px">{base_path}/{version}/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiOrchestrateV3Agents(page, limit, callback)</td>
    <td style="padding:15px">Retrieve all agents</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiOrchestrateV3AgentsAgentId(agentId, callback)</td>
    <td style="padding:15px">Retrieve an agent by agentId.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiOrchestrateV3AgentsAgentId(agentId, callback)</td>
    <td style="padding:15px">Delete an agent specified by the provided agentId.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiOrchestrateV3AgentsConfiguration(page, limit, body, callback)</td>
    <td style="padding:15px">Create an agent configuration</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiOrchestrateV3AgentsConfiguration(page, limit, callback)</td>
    <td style="padding:15px">Retrieve all agent configurations</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiOrchestrateV3AgentsConfigurationAgentId(agentId, callback)</td>
    <td style="padding:15px">Retrieve an agent configuration by agentId.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putApiOrchestrateV3AgentsConfigurationAgentId(agentId, body, callback)</td>
    <td style="padding:15px">Update an agent configuration specified by the provided agentId.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiOrchestrateV3AgentsConfigurationAgentId(agentId, callback)</td>
    <td style="padding:15px">Delete an agent configuration specified by the provided agentId.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiOrchestrateV3AgentsAgentIdCommand(agentId, body, callback)</td>
    <td style="padding:15px">Sends a given command to be run for the agent specified by agentId</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/{pathv1}/command?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiOrchestrateV3AgentsSessions(page, limit, callback)</td>
    <td style="padding:15px">Retrieve all sessions</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiOrchestrateV3AgentsSessions(page, limit, body, callback)</td>
    <td style="padding:15px">Create agent sessions</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiOrchestrateV3AgentsSession(body, callback)</td>
    <td style="padding:15px">Create an agent session</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putApiOrchestrateV3AgentsSession(body, callback)</td>
    <td style="padding:15px">Update an agent session.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiOrchestrateV3AgentsSessionSessionId(sessionId, callback)</td>
    <td style="padding:15px">Retrieve an agent session by sessionId.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/session/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiOrchestrateV3AgentsSessionSessionId(sessionId, callback)</td>
    <td style="padding:15px">Delete an agent session by sessionId.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/session/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiOrchestrateV3AgentsSessionsAgentId(agentId, callback)</td>
    <td style="padding:15px">Retrieve agent sessions by agentId.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiOrchestrateV3AgentsSessionsAgentId(agentId, callback)</td>
    <td style="padding:15px">Delete all sessions for the provided agentId.</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiOrchestrateV3AgentsSessionstatusSessionId(sessionId, callback)</td>
    <td style="padding:15px">Retrieves the session status for session with sessionId</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/sessionstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiOrchestrateV3AgentsAgentIdSecrets(agentId, callback)</td>
    <td style="padding:15px">Generates and returns the agent secrets required to start an agent</td>
    <td style="padding:15px">{base_path}/{version}/orchestrate/v3/agents/{pathv1}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">openIDConfiguration(clientName, callback)</td>
    <td style="padding:15px">OpenIDConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/openid-configuration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">openIDToken(clientName, grantType = 'authorization_code', code, clientId, clientSecret, callback)</td>
    <td style="padding:15px">OpenIDToken</td>
    <td style="padding:15px">{base_path}/{version}/openid-token/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">openIDAuth(clientId, redirectUri, responseType, state, delegatedclientid, scope, callback)</td>
    <td style="padding:15px">OpenIDAuth</td>
    <td style="padding:15px">{base_path}/{version}/openid-auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">samlRedirect(clientName, callback)</td>
    <td style="padding:15px">SamlRedirect</td>
    <td style="padding:15px">{base_path}/{version}/saml-redirect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">samlCallback(sAMLResponse, relayState, callback)</td>
    <td style="padding:15px">SamlCallback</td>
    <td style="padding:15px">{base_path}/{version}/saml-callback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">samlSpMetadata(clientName, callback)</td>
    <td style="padding:15px">SamlSpMetadata</td>
    <td style="padding:15px">{base_path}/{version}/saml-metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGrantingTicket(username, password, service = 'https://cas.local/aastubs/', callback)</td>
    <td style="padding:15px">CreateGrantingTicket</td>
    <td style="padding:15px">{base_path}/{version}/v1/grant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServiceTicket(grantingTicket, service = 'https://cas.local/aastubs/', callback)</td>
    <td style="padding:15px">CreateServiceTicket</td>
    <td style="padding:15px">{base_path}/{version}/v1/service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateServiceTicket(ticket, service = 'https://cas.local/aastubs/', format = 'JSON', callback)</td>
    <td style="padding:15px">ValidateServiceTicket</td>
    <td style="padding:15px">{base_path}/{version}/v1/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">CreateUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsers(callback)</td>
    <td style="padding:15px">GetAllUsers</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(userId, body, callback)</td>
    <td style="padding:15px">UpdateUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserProperties(userId, body, callback)</td>
    <td style="padding:15px">UpdateUserProperties</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(userId, callback)</td>
    <td style="padding:15px">GetUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(userId, callback)</td>
    <td style="padding:15px">DeleteUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetPassword(userId, callback)</td>
    <td style="padding:15px">ResetPassword</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/users/{pathv1}/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeEmailAddress(userId, body, callback)</td>
    <td style="padding:15px">ChangeEmailAddress</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/users/{pathv1}/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthenticatedUserProperties(body, callback)</td>
    <td style="padding:15px">UpdateAuthenticatedUserProperties</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticatedUser(callback)</td>
    <td style="padding:15px">GetAuthenticatedUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthenticatedUser(callback)</td>
    <td style="padding:15px">DeleteAuthenticatedUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthenticatedUserPassword(body, callback)</td>
    <td style="padding:15px">UpdateAuthenticatedUserPassword</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/me/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeAuthenticatedUserEmailAddress(body, callback)</td>
    <td style="padding:15px">ChangeAuthenticatedUserEmailAddress</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/me/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserByToken(onboardingToken, callback)</td>
    <td style="padding:15px">GetUserByToken</td>
    <td style="padding:15px">{base_path}/{version}/v1/onboarding/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setUserPassword(body, callback)</td>
    <td style="padding:15px">SetUserPassword</td>
    <td style="padding:15px">{base_path}/{version}/v1/onboarding/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">login(username, password, token, callback)</td>
    <td style="padding:15px">Login</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logout(callback)</td>
    <td style="padding:15px">Logout</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validate(callback)</td>
    <td style="padding:15px">Validate</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateSessionForProxy(callback)</td>
    <td style="padding:15px">ValidateSessionForProxy</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createService(serviceName, username, password, callback)</td>
    <td style="padding:15px">CreateService</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantInfo(callback)</td>
    <td style="padding:15px">GetTenantInfo</td>
    <td style="padding:15px">{base_path}/{version}/v1/onboarding/tenant-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setTenantIdentity(body, callback)</td>
    <td style="padding:15px">SetTenantIdentity</td>
    <td style="padding:15px">{base_path}/{version}/v1/onboarding/tenant-identity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenantIdentity(subdomain, callback)</td>
    <td style="padding:15px">DeleteTenantIdentity</td>
    <td style="padding:15px">{base_path}/{version}/v1/onboarding/tenant-identity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createToken(body, callback)</td>
    <td style="padding:15px">CreateToken</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTokens(callback)</td>
    <td style="padding:15px">GetAllTokens</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getToken(tokenId, callback)</td>
    <td style="padding:15px">GetToken</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateToken(tokenId, body, callback)</td>
    <td style="padding:15px">UpdateToken</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteToken(tokenId, callback)</td>
    <td style="padding:15px">DeleteToken</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationMethod(emailAddress, callback)</td>
    <td style="padding:15px">GetAuthenticationMethod</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/method/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUsergroup(body, callback)</td>
    <td style="padding:15px">CreateUsergroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsergroups(callback)</td>
    <td style="padding:15px">GetAllUsergroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergroup(usergroupId, callback)</td>
    <td style="padding:15px">GetUsergroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUsergroup(usergroupId, body, callback)</td>
    <td style="padding:15px">UpdateUsergroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsergroup(usergroupId, callback)</td>
    <td style="padding:15px">DeleteUsergroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRole(body, callback)</td>
    <td style="padding:15px">CreateRole</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRoles(callback)</td>
    <td style="padding:15px">GetAllRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRole(roleId, callback)</td>
    <td style="padding:15px">GetRole</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRole(roleId, body, callback)</td>
    <td style="padding:15px">UpdateRole</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRole(roleId, callback)</td>
    <td style="padding:15px">DeleteRole</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPermission(body, callback)</td>
    <td style="padding:15px">CreatePermission</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPermissions(domains, app, callback)</td>
    <td style="padding:15px">GetAllPermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermission(permissionId, callback)</td>
    <td style="padding:15px">GetPermission</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePermission(permissionId, body, callback)</td>
    <td style="padding:15px">UpdatePermission</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermission(permissionId, callback)</td>
    <td style="padding:15px">DeletePermission</td>
    <td style="padding:15px">{base_path}/{version}/v1/auth/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createScheduledExport(body, callback)</td>
    <td style="padding:15px">CreateScheduledExport</td>
    <td style="padding:15px">{base_path}/{version}/v1/export/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllExportConfigurations(includeDoc, rawMetadataKey, enabled, callback)</td>
    <td style="padding:15px">GetAllExportConfigurations</td>
    <td style="padding:15px">{base_path}/{version}/v1/export/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExportConfiguration(confId, rawMetadataKey, callback)</td>
    <td style="padding:15px">GetExportConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/export/configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExportConfiguration(confId, body, callback)</td>
    <td style="padding:15px">PatchExportConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/export/configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableExportConfiguration(confId, callback)</td>
    <td style="padding:15px">DisableExportConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/export/configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllExportReport(confId, callback)</td>
    <td style="padding:15px">GetAllExportReport</td>
    <td style="padding:15px">{base_path}/{version}/v1/export/{pathv1}/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExportReport(confId, fileName, callback)</td>
    <td style="padding:15px">GetExportReport</td>
    <td style="padding:15px">{base_path}/{version}/v1/export/{pathv1}/report/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSolutionManifest(callback)</td>
    <td style="padding:15px">GetSolutionManifest</td>
    <td style="padding:15px">{base_path}/{version}/v1/solution-version-manifest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTimeExclusion(body, callback)</td>
    <td style="padding:15px">CreateTimeExclusion</td>
    <td style="padding:15px">{base_path}/{version}/v3/twe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTimeExclusion(callback)</td>
    <td style="padding:15px">GetAllTimeExclusion</td>
    <td style="padding:15px">{base_path}/{version}/v3/twe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTimeExclusion(id, body, callback)</td>
    <td style="padding:15px">UpdateTimeExclusion</td>
    <td style="padding:15px">{base_path}/{version}/v3/twe/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeExclusion(id, callback)</td>
    <td style="padding:15px">GetTimeExclusion</td>
    <td style="padding:15px">{base_path}/{version}/v3/twe/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTimeExclusion(id, callback)</td>
    <td style="padding:15px">DeleteTimeExclusion</td>
    <td style="padding:15px">{base_path}/{version}/v3/twe/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">timeExclusionEvents(start, end, monitoredObjectID, callback)</td>
    <td style="padding:15px">TimeExclusionEvents</td>
    <td style="padding:15px">{base_path}/{version}/v3/twe/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTenantV2(body, callback)</td>
    <td style="padding:15px">CreateTenantV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTenantsV2(callback)</td>
    <td style="padding:15px">GetAllTenantsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantV2(tenantId, callback)</td>
    <td style="padding:15px">GetTenantV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenantV2(tenantId, callback)</td>
    <td style="padding:15px">DeleteTenantV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTenantV2(tenantId, body, callback)</td>
    <td style="padding:15px">PatchTenantV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantIdByAliasV2(value, callback)</td>
    <td style="padding:15px">GetTenantIdByAliasV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant-by-alias/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantSummaryByAliasV2(value, callback)</td>
    <td style="padding:15px">GetTenantSummaryByAliasV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant-summary-by-alias/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIngestionDictionaryV2(callback)</td>
    <td style="padding:15px">GetIngestionDictionaryV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/ingestion-dictionaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getValidTypesV2(callback)</td>
    <td style="padding:15px">GetValidTypesV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/valid-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantMetadataV2(tenantId, callback)</td>
    <td style="padding:15px">GetTenantMetadataV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant-metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTenantMetadataV2(tenantId, body, callback)</td>
    <td style="padding:15px">UpdateTenantMetadataV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/tenant-metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertsForMonitoredObject(monitoredObjectId, interval, alertState = 'raised', policyId, callback)</td>
    <td style="padding:15px">getAlertsForMonitoredObject</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/monitoredObjectIds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveAlertsForMonitoredObject(monitoredObjectId, policyId, callback)</td>
    <td style="padding:15px">getActiveAlertsForMonitoredObject</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/active/monitoredObjectIds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteActiveAlertsForMonitoredObject(monitoredObjectId, policyId, callback)</td>
    <td style="padding:15px">deleteActiveAlertsForMonitoredObject</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/active/monitoredObjectIds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createActiveAlertForMonitoredObject(body, callback)</td>
    <td style="padding:15px">createActiveAlertForMonitoredObject</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/active/monitoredObjectIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveAlertsForPolicy(policyId, callback)</td>
    <td style="padding:15px">getActiveAlertsForPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/active/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteActiveAlertsForPolicy(policyId, monitoredObjectId, callback)</td>
    <td style="padding:15px">deleteActiveAlertsForPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/active/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveAlertGroupBy(body, callback)</td>
    <td style="padding:15px">getActiveAlertGroupBy</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/active/groupBy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveAlertsForMetaFilter(body, callback)</td>
    <td style="padding:15px">getActiveAlertsForMetaFilter</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveAlertsFilteredCounts(body, callback)</td>
    <td style="padding:15px">getActiveAlertsFilteredCounts</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/active/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateActiveAlertsSecurityState(body, callback)</td>
    <td style="padding:15px">updateActiveAlertsSecurityState</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/active/securityState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRequestAlertPolicy(body, callback)</td>
    <td style="padding:15px">createRequestAlertPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/alerting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRequestAlertPolicy(useRawMetaKey, includeDeleted, policyType, callback)</td>
    <td style="padding:15px">getAllRequestAlertPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/alerting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAutomaticAlertPolicy(body, callback)</td>
    <td style="padding:15px">patchAutomaticAlertPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/alerting/automatic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutomaticAlertPolicy(callback)</td>
    <td style="padding:15px">getAutomaticAlertPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/alerting/automatic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRequestAlertPolicy(id, useRawMetaKey, callback)</td>
    <td style="padding:15px">getRequestAlertPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/alerting/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRequestAlertPolicy(id, callback)</td>
    <td style="padding:15px">deleteRequestAlertPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/alerting/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRequestAlertPolicy(id, force = 'false', body, callback)</td>
    <td style="padding:15px">PatchRequestAlertPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/alerting/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertPolicyHistory(policyId, interval, monitoredObjectId, alertState = 'raised', policyType = 'micro-tca', limit, useRawMetaKey, category, callback)</td>
    <td style="padding:15px">getAlertPolicyHistory</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/history/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertingGroupByV3(body, callback)</td>
    <td style="padding:15px">GetAlertingGroupByV3</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerting/groupBy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertingAggregateV3(body, callback)</td>
    <td style="padding:15px">GetAlertingAggregateV3</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerting/aggregate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRamenState(tenantId, callback)</td>
    <td style="padding:15px">getRamenState</td>
    <td style="padding:15px">{base_path}/{version}/v2/ramen/state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAnalyticsV2(body, callback)</td>
    <td style="padding:15px">CreateAnalyticsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/analytics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAnalyticsV2(callback)</td>
    <td style="padding:15px">GetAllAnalyticsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/analytics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnalyticsV2(analyticsId, callback)</td>
    <td style="padding:15px">GetAnalyticsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/analytics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnalyticsV2(analyticsId, callback)</td>
    <td style="padding:15px">DeleteAnalyticsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/analytics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBrandingV2(body, callback)</td>
    <td style="padding:15px">CreateBrandingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/brandings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllBrandingsV2(zone, callback)</td>
    <td style="padding:15px">GetAllBrandingsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/brandings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBrandingV2(brandingId, callback)</td>
    <td style="padding:15px">GetBrandingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/brandings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBrandingV2(brandingId, body, callback)</td>
    <td style="padding:15px">UpdateBrandingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/brandings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBrandingV2(brandingId, callback)</td>
    <td style="padding:15px">DeleteBrandingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/brandings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLocaleV2(body, callback)</td>
    <td style="padding:15px">CreateLocaleV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/locales?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllLocalesV2(callback)</td>
    <td style="padding:15px">GetAllLocalesV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/locales?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocaleV2(localeId, callback)</td>
    <td style="padding:15px">GetLocaleV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/locales/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLocaleV2(localeId, body, callback)</td>
    <td style="padding:15px">UpdateLocaleV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/locales/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLocaleV2(localeId, callback)</td>
    <td style="padding:15px">DeleteLocaleV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/locales/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCompositeMetricV2(body, callback)</td>
    <td style="padding:15px">CreateCompositeMetricV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/composite-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCompositeMetricsV2(callback)</td>
    <td style="padding:15px">GetAllCompositeMetricsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/composite-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCompositeMetricV2(compositeMetricId, callback)</td>
    <td style="padding:15px">GetCompositeMetricV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/composite-metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCompositeMetricV2(compositeMetricId, body, callback)</td>
    <td style="padding:15px">UpdateCompositeMetricV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/composite-metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCompositeMetricV2(compositeMetricId, callback)</td>
    <td style="padding:15px">DeleteCompositeMetricV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/composite-metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConnectorConfigV2(body, callback)</td>
    <td style="padding:15px">CreateConnectorConfigV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllConnectorConfigsV2(zone, callback)</td>
    <td style="padding:15px">GetAllConnectorConfigsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectorConfigV2(connectorId, callback)</td>
    <td style="padding:15px">GetConnectorConfigV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConnectorConfigV2(connectorId, body, callback)</td>
    <td style="padding:15px">UpdateConnectorConfigV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConnectorConfigV2(connectorId, callback)</td>
    <td style="padding:15px">DeleteConnectorConfigV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConnectorInstanceV2(body, callback)</td>
    <td style="padding:15px">CreateConnectorInstanceV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllConnectorInstancesV2(callback)</td>
    <td style="padding:15px">GetAllConnectorInstancesV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectorInstanceV2(connectorInstanceId, callback)</td>
    <td style="padding:15px">GetConnectorInstanceV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConnectorInstanceV2(connectorInstanceId, body, callback)</td>
    <td style="padding:15px">UpdateConnectorInstanceV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConnectorInstanceV2(connectorInstanceId, callback)</td>
    <td style="padding:15px">DeleteConnectorInstanceV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/connector-instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDashboardV2(body, callback)</td>
    <td style="padding:15px">CreateDashboardV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/dashboards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDashboardsV2(zone, callback)</td>
    <td style="padding:15px">GetAllDashboardsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/dashboards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardV2(dashboardId, callback)</td>
    <td style="padding:15px">GetDashboardV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDashboardV2(dashboardId, body, callback)</td>
    <td style="padding:15px">UpdateDashboardV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDashboardV2(dashboardId, callback)</td>
    <td style="padding:15px">DeleteDashboardV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/dashboards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCardV2(body, callback)</td>
    <td style="padding:15px">CreateCardV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCardsV2(zone, callback)</td>
    <td style="padding:15px">GetAllCardsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCardV2(cardId, callback)</td>
    <td style="padding:15px">GetCardV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCardV2(cardId, body, callback)</td>
    <td style="padding:15px">UpdateCardV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCardV2(cardId, callback)</td>
    <td style="padding:15px">DeleteCardV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDataCleaningProfile(body, callback)</td>
    <td style="padding:15px">CreateDataCleaningProfile</td>
    <td style="padding:15px">{base_path}/{version}/v2/data-cleaning-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataCleaningProfiles(callback)</td>
    <td style="padding:15px">GetDataCleaningProfiles</td>
    <td style="padding:15px">{base_path}/{version}/v2/data-cleaning-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataCleaningProfile(profileId, callback)</td>
    <td style="padding:15px">GetDataCleaningProfile</td>
    <td style="padding:15px">{base_path}/{version}/v2/data-cleaning-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDataCleaningProfile(profileId, body, callback)</td>
    <td style="padding:15px">UpdateDataCleaningProfile</td>
    <td style="padding:15px">{base_path}/{version}/v2/data-cleaning-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDataCleaningProfile(profileId, callback)</td>
    <td style="padding:15px">DeleteDataCleaningProfile</td>
    <td style="padding:15px">{base_path}/{version}/v2/data-cleaning-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataCleaningHistory(monitoredObjectId, interval, callback)</td>
    <td style="padding:15px">GetDataCleaningHistory</td>
    <td style="padding:15px">{base_path}/{version}/v2/data-cleaning-history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadRoadrunner(zone, callback)</td>
    <td style="padding:15px">DownloadRoadrunner</td>
    <td style="padding:15px">{base_path}/{version}/v2/distribution/download-roadrunner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIngestionProfileV2(body, callback)</td>
    <td style="padding:15px">CreateIngestionProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/ingestion-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIngestionProfilesV2(callback)</td>
    <td style="padding:15px">GetAllIngestionProfilesV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/ingestion-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIngestionProfileV2(ingestionProfileId, callback)</td>
    <td style="padding:15px">GetIngestionProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/ingestion-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIngestionProfileV2(ingestionProfileId, body, callback)</td>
    <td style="padding:15px">UpdateIngestionProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/ingestion-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIngestionProfileV2(ingestionProfileId, callback)</td>
    <td style="padding:15px">DeleteIngestionProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/ingestion-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMetadataConfigV2(body, callback)</td>
    <td style="padding:15px">CreateMetadataConfigV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/metadata-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllMetadataConfigsV2(callback)</td>
    <td style="padding:15px">GetAllMetadataConfigsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/metadata-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetadataConfigV2(metadataConfigId, callback)</td>
    <td style="padding:15px">GetMetadataConfigV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/metadata-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMetadataConfigV2(metadataConfigId, body, callback)</td>
    <td style="padding:15px">UpdateMetadataConfigV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/metadata-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMetadataConfigV2(metadataConfigId, callback)</td>
    <td style="padding:15px">DeleteMetadataConfigV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/metadata-configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetricBaselineByMonitoredObjectIdV2(monitoredObjectId, interval, directions, metrics, callback)</td>
    <td style="padding:15px">GetMetricBaselineByMonitoredObjectIdV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/metric-baselines/by-monitored-object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDuplicateMonitoredObjectReportsV2(callback)</td>
    <td style="padding:15px">GetAllDuplicateMonitoredObjectReportsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/duplicate-monitored-object-reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateDuplicateMonitoredObjectReportV2(callback)</td>
    <td style="padding:15px">GenerateDuplicateMonitoredObjectReportV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/duplicate-monitored-object-reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDuplicateMonitoredObjectReportV2(duplicateMonitoredObjectResportId, callback)</td>
    <td style="padding:15px">GetDuplicateMonitoredObjectReportV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/duplicate-monitored-object-reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectTagMappingV2(body, callback)</td>
    <td style="padding:15px">CreateMonitoredObjectTagMappingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-tag-mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllMonitoredObjectTagMappingsV2(zone, callback)</td>
    <td style="padding:15px">GetAllMonitoredObjectTagMappingsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-tag-mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectTagMappingV2(monitoredObjectTagMappingId, callback)</td>
    <td style="padding:15px">GetMonitoredObjectTagMappingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-tag-mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitoredObjectTagMappingV2(monitoredObjectTagMappingId, body, callback)</td>
    <td style="padding:15px">UpdateMonitoredObjectTagMappingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-tag-mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitoredObjectTagMappingV2(monitoredObjectTagMappingId, callback)</td>
    <td style="padding:15px">DeleteMonitoredObjectTagMappingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-tag-mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllMonitoredObjectsV2(startKey, limit, callback)</td>
    <td style="padding:15px">GetAllMonitoredObjectsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitoredObjectV2(body, callback)</td>
    <td style="padding:15px">CreateMonitoredObjectV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredMonitoredObjectCountV2(groupBy = 'object-type', body, callback)</td>
    <td style="padding:15px">GetFilteredMonitoredObjectCountV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-objects/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredMonitoredObjectListV2(body, callback)</td>
    <td style="padding:15px">GetFilteredMonitoredObjectListV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-objects/id-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectReportingListV2(body, callback)</td>
    <td style="padding:15px">GetMonitoredObjectReportingListV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-objects/reporting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectGeoClustersV2(precision, location = 'source', body, callback)</td>
    <td style="padding:15px">GetMonitoredObjectGeoClustersV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-geo-clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectMetadataDetailsV2(meta, valuePrefix, categoriesOnly, callback)</td>
    <td style="padding:15px">GetMonitoredObjectMetadataDetailsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-meta-details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredMonitoredObjectMetadataDetailsV2(body, callback)</td>
    <td style="padding:15px">GetFilteredMonitoredObjectMetadataDetailsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-meta-details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectV2(monObjId, callback)</td>
    <td style="padding:15px">GetMonitoredObjectV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitoredObjectV2(monObjId, body, callback)</td>
    <td style="padding:15px">UpdateMonitoredObjectV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitoredObjectV2(monObjId, callback)</td>
    <td style="padding:15px">DeleteMonitoredObjectV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkPatchMonitoredObjectsV2(body, callback)</td>
    <td style="padding:15px">BulkPatchMonitoredObjectsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/bulk/patch/monitored-objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkInsertMonitoredObjectsV2(body, callback)</td>
    <td style="padding:15px">BulkInsertMonitoredObjectsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/bulk/insert/monitored-objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkUpdateMonitoredObjectsV2(body, callback)</td>
    <td style="padding:15px">BulkUpdateMonitoredObjectsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/bulk/insert/monitored-objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkInsertMonitoredObjectsMetaV2(body, callback)</td>
    <td style="padding:15px">BulkInsertMonitoredObjectsMetaV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/bulk/insert/monitored-objects/meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllMonitoredObjectSummariesV2(startKey, limit, callback)</td>
    <td style="padding:15px">GetAllMonitoredObjectSummariesV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-summaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoredObjectSummaryV2(monObjId, callback)</td>
    <td style="padding:15px">GetMonitoredObjectSummaryV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/monitored-object-summaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllMetadataCategoryMappingsV2(callback)</td>
    <td style="padding:15px">GetAllMetadataCategoryMappingsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/metadata-category-mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetadataCategoryMappingV2(mappingType = 'unknown', callback)</td>
    <td style="padding:15px">GetMetadataCategoryMappingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/metadata-category-mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMetadataCategoryMappingV2(mappingType = 'unknown', body, callback)</td>
    <td style="padding:15px">UpdateMetadataCategoryMappingV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/metadata-category-mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSessionFilterProfileV2(body, callback)</td>
    <td style="padding:15px">CreateSessionFilterProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filter-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSessionFilterProfilesV2(callback)</td>
    <td style="padding:15px">GetAllSessionFilterProfilesV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filter-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionFilterProfileV2(sessionFilterProfileId, callback)</td>
    <td style="padding:15px">GetSessionFilterProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filter-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSessionFilterProfileV2(sessionFilterProfileId, body, callback)</td>
    <td style="padding:15px">UpdateSessionFilterProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filter-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSessionFilterProfileV2(sessionFilterProfileId, body, callback)</td>
    <td style="padding:15px">PatchSessionFilterProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filter-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSessionFilterProfileV2(sessionFilterProfileId, callback)</td>
    <td style="padding:15px">DeleteSessionFilterProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filter-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSessionFilterV2(body, callback)</td>
    <td style="padding:15px">CreateSessionFilterV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSessionFiltersV2(callback)</td>
    <td style="padding:15px">GetAllSessionFiltersV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionFilterV2(sessionFilterId, callback)</td>
    <td style="padding:15px">GetSessionFilterV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSessionFilterV2(sessionFilterId, body, callback)</td>
    <td style="padding:15px">UpdateSessionFilterV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSessionFilterV2(sessionFilterId, body, callback)</td>
    <td style="padding:15px">PatchSessionFilterV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSessionFilterV2(sessionFilterId, callback)</td>
    <td style="padding:15px">DeleteSessionFilterV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/session-filters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createThresholdProfileV2(body, callback)</td>
    <td style="padding:15px">CreateThresholdProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/threshold-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllThresholdProfilesV2(callback)</td>
    <td style="padding:15px">GetAllThresholdProfilesV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/threshold-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholdProfileV2(thrPrfId, callback)</td>
    <td style="padding:15px">GetThresholdProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/threshold-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateThresholdProfileV2(thrPrfId, body, callback)</td>
    <td style="padding:15px">UpdateThresholdProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/threshold-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThresholdProfileV2(thrPrfId, callback)</td>
    <td style="padding:15px">DeleteThresholdProfileV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/threshold-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkAssetV2(body, callback)</td>
    <td style="padding:15px">CreateNetworkAssetV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cyber/network-assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllNetworkAssetsV2(callback)</td>
    <td style="padding:15px">GetAllNetworkAssetsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cyber/network-assets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAssetV2(networkAssetId, callback)</td>
    <td style="padding:15px">GetNetworkAssetV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cyber/network-assets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkAssetV2(networkAssetId, body, callback)</td>
    <td style="padding:15px">UpdateNetworkAssetV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cyber/network-assets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkAssetV2(networkAssetId, callback)</td>
    <td style="padding:15px">DeleteNetworkAssetV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cyber/network-assets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredNetworkAssetCountV2(body, callback)</td>
    <td style="padding:15px">GetFilteredNetworkAssetCountV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cyber/network-assets/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredNetworkAssetListV2(body, callback)</td>
    <td style="padding:15px">GetFilteredNetworkAssetListV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cyber/network-assets/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAssetGroupByV2(body, callback)</td>
    <td style="padding:15px">GetNetworkAssetGroupByV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/cyber/network-assets/groupBy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bootstrapsthemonitoredobjectcacheforalltenants(callback)</td>
    <td style="padding:15px">Bootstraps the monitored object cache for all tenants</td>
    <td style="padding:15px">{base_path}/{version}/v2/manager/monitoredObjectCache/build?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invokesbaselinebulkupdate(callback)</td>
    <td style="padding:15px">Invokes baseline bulk update</td>
    <td style="padding:15px">{base_path}/{version}/v2/manager/baseline/bulkUpdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purgesanalyticsreports(callback)</td>
    <td style="padding:15px">Purges analytics reports</td>
    <td style="padding:15px">{base_path}/{version}/v2/manager/analytics/purgeRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bootstrapsthenetworkassetcacheforalltenants(callback)</td>
    <td style="padding:15px">Bootstraps the network asset cache for all tenants</td>
    <td style="padding:15px">{base_path}/{version}/v2/manager/networkAssetCache/build?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIngestionDictionaryV3(body, callback)</td>
    <td style="padding:15px">CreateIngestionDictionaryV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/ingestion-dictionaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIngestionDictionariesV3(callback)</td>
    <td style="padding:15px">GetAllIngestionDictionariesV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/ingestion-dictionaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIngestionDictionaryV3(ingestionDictionaryId, callback)</td>
    <td style="padding:15px">GetIngestionDictionaryV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/ingestion-dictionaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIngestionDictionaryV3(ingestionDictionaryId, body, callback)</td>
    <td style="padding:15px">UpdateIngestionDictionaryV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/ingestion-dictionaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIngestionDictionaryV3(ingestionDictionaryId, callback)</td>
    <td style="padding:15px">DeleteIngestionDictionaryV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/ingestion-dictionaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDatasourceReconciliationTasksV3(datasource, taskCount, body, callback)</td>
    <td style="padding:15px">CreateDatasourceReconciliationTasksV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/metrics-admin/reconciliation/datasource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDatasourceReindexTasksV3(datasource, taskCount, periodOffsetMultiplier, body, callback)</td>
    <td style="padding:15px">CreateDatasourceReindexTasksV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/metrics-admin/reindex/datasource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupByV3(body, callback)</td>
    <td style="padding:15px">GetGroupByV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/metrics/groupBy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAggregateV3(body, callback)</td>
    <td style="padding:15px">GetAggregateV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/metrics/aggregate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTcpThroughputTestReportV3(body, callback)</td>
    <td style="padding:15px">CreateTcpThroughputTestReportV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/tcpThroughput/tests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTcpThroughputTestReportsV3(shallow, token, callback)</td>
    <td style="padding:15px">GetAllTcpThroughputTestReportsV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/tcpThroughput/tests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTcpThroughputReportsForTestV3(testId, shallow, token, callback)</td>
    <td style="padding:15px">GetAllTcpThroughputReportsForTestV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/tcpThroughput/tests/{pathv1}/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTcpThroughputTestReportV3(testId, runId, shallow, callback)</td>
    <td style="padding:15px">GetTcpThroughputTestReportV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/tcpThroughput/tests/{pathv1}/reports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTcpThroughputTestReportV3(testId, runId, body, callback)</td>
    <td style="padding:15px">UpdateTcpThroughputTestReportV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/tcpThroughput/tests/{pathv1}/reports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTcpThroughputTestReportV3(testId, runId, callback)</td>
    <td style="padding:15px">DeleteTcpThroughputTestReportV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/tcpThroughput/tests/{pathv1}/reports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredTcpThroughputCountV3(body, callback)</td>
    <td style="padding:15px">GetFilteredTcpThroughputCountV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/tcpThroughput/tests/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredTcpThroughputListV3(shallow, token, body, callback)</td>
    <td style="padding:15px">GetFilteredTcpThroughputListV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/tcpThroughput/tests/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
