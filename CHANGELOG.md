
## 0.6.9 [10-15-2024]

* Changes made at 2024.10.14_20:57PM

See merge request itentialopensource/adapters/adapter-accedian_skylight!19

---

## 0.6.8 [08-21-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-accedian_skylight!17

---

## 0.6.7 [08-14-2024]

* Changes made at 2024.08.14_19:16PM

See merge request itentialopensource/adapters/adapter-accedian_skylight!16

---

## 0.6.6 [08-07-2024]

* Changes made at 2024.08.06_20:51PM

See merge request itentialopensource/adapters/adapter-accedian_skylight!15

---

## 0.6.5 [08-05-2024]

* Changes made at 2024.08.05_14:45PM

See merge request itentialopensource/adapters/adapter-accedian_skylight!14

---

## 0.6.4 [03-27-2024]

* Changes made at 2024.03.27_13:20PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!12

---

## 0.6.3 [03-12-2024]

* Changes made at 2024.03.12_11:02AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!10

---

## 0.6.2 [02-27-2024]

* Changes made at 2024.02.27_11:38AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!9

---

## 0.6.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!8

---

## 0.6.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!7

---

## 0.5.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!7

---

## 0.4.0 [10-19-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!6

---

## 0.3.0 [09-26-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!6

---

## 0.2.0 [05-29-2022]

* Migration to the latest Adapter Foundation and add orchestration calls

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!4

---

## 0.1.4 [02-25-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!3

---

## 0.1.3 [07-06-2020] & 0.1.2 [07-02-2020]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!2

---

## 0.1.1 [05-07-2020]

- Fixed a few input types from string to object

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!1

---
