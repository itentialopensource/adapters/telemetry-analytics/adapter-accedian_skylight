# Accedian Skylight

Vendor: Accedian (acquired by Cisco)
Homepage: https://www.cisco.com/

Product: Skylight
Product Page: https://www.cisco.com/site/us/en/products/networking/software/provider-connectivity-assurance/index.html

## Introduction
We classify Accedian Skylight into the Service Assurance domain as Accedian Skylight provides tools and endpoints for monitoring and managing network performance and configurations. 

"Skylight generates billions of unique performance measurements per day, providing excellent visibility into your network, service and application performance"

## Why Integrate
The Accedian Skylight adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Accedian Skylight. With this adapter you have the ability to perform operations such as:

- Manage configuration and sensor orchestration service
- Get Endpoint
- Create Reflector Endpoint
- Perform Operation on Endpoint
- Get, Create, or Delete SLA

## Additional Product Documentation
The [API documents for Accedian Skylight](https://api.analytics.accedian.io/)