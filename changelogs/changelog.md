
## 0.2.0 [05-29-2022]

* Migration to the latest Adapter Foundation and add orchestration calls

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!4

---

## 0.1.4 [02-25-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!3

---

## 0.1.3 [07-06-2020] & 0.1.2 [07-02-2020]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!2

---

## 0.1.1 [05-07-2020]

- Fixed a few input types from string to object

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight!1

---
