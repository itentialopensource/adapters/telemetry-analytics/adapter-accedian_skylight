/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-accedian_skylight',
      type: 'AccedianSkylight',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const AccedianSkylight = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Accedian_skylight Adapter Test', () => {
  describe('AccedianSkylight Class Tests', () => {
    const a = new AccedianSkylight(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('accedian_skylight'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('accedian_skylight'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('AccedianSkylight', pronghornDotJson.export);
          assert.equal('Accedian_skylight', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-accedian_skylight', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('accedian_skylight'));
          assert.equal('AccedianSkylight', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-accedian_skylight', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-accedian_skylight', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getEndpoints - errors', () => {
      it('should have a getEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.getEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEndpoint - errors', () => {
      it('should have a deleteEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteEndpoint(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alterEndpointDescription - errors', () => {
      it('should have a alterEndpointDescription function', (done) => {
        try {
          assert.equal(true, typeof a.alterEndpointDescription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.alterEndpointDescription(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-alterEndpointDescription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndpointInformation - errors', () => {
      it('should have a getEndpointInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getEndpointInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getEndpointInformation(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getEndpointInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeNameOfEndpoint - errors', () => {
      it('should have a changeNameOfEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.changeNameOfEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeNameOfEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-changeNameOfEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#operationOnEndpoint - errors', () => {
      it('should have a operationOnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.operationOnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.operationOnEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-operationOnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInformationOnEndpointsByType - errors', () => {
      it('should have a getInformationOnEndpointsByType function', (done) => {
        try {
          assert.equal(true, typeof a.getInformationOnEndpointsByType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getInformationOnEndpointsByType(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getInformationOnEndpointsByType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNewReflectorEndpoint - errors', () => {
      it('should have a createNewReflectorEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.createNewReflectorEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReflectorEndpointByName - errors', () => {
      it('should have a getReflectorEndpointByName function', (done) => {
        try {
          assert.equal(true, typeof a.getReflectorEndpointByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getReflectorEndpointByName(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getReflectorEndpointByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeIpAddressUnmanagedEndpoint - errors', () => {
      it('should have a changeIpAddressUnmanagedEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.changeIpAddressUnmanagedEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeIpAddressUnmanagedEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-changeIpAddressUnmanagedEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeCapabilityUnmanagedEndpoint - errors', () => {
      it('should have a changeCapabilityUnmanagedEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.changeCapabilityUnmanagedEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeCapabilityUnmanagedEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-changeCapabilityUnmanagedEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReflectorEndpointByMAC - errors', () => {
      it('should have a getReflectorEndpointByMAC function', (done) => {
        try {
          assert.equal(true, typeof a.getReflectorEndpointByMAC === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getReflectorEndpointByMAC(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getReflectorEndpointByMAC', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeTwampControlProtocolSettings - errors', () => {
      it('should have a changeTwampControlProtocolSettings function', (done) => {
        try {
          assert.equal(true, typeof a.changeTwampControlProtocolSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeTwampControlProtocolSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-changeTwampControlProtocolSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTwampCp - errors', () => {
      it('should have a deleteTwampCp function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTwampCp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteTwampCp(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteTwampCp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSupervisonEndpoint - errors', () => {
      it('should have a createSupervisonEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.createSupervisonEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSupervisionEndpointByName - errors', () => {
      it('should have a getSupervisionEndpointByName function', (done) => {
        try {
          assert.equal(true, typeof a.getSupervisionEndpointByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSupervisionEndpointByName(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getSupervisionEndpointByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionInformation - errors', () => {
      it('should have a getSessionInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSession - errors', () => {
      it('should have a deleteSession function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteSession(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alterSessionDescription - errors', () => {
      it('should have a alterSessionDescription function', (done) => {
        try {
          assert.equal(true, typeof a.alterSessionDescription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.alterSessionDescription(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-alterSessionDescription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionHeadInformationByName - errors', () => {
      it('should have a getSessionHeadInformationByName function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionHeadInformationByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSessionHeadInformationByName(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getSessionHeadInformationByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionLastRR - errors', () => {
      it('should have a getSessionLastRR function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionLastRR === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSessionLastRR(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getSessionLastRR', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alterTerminatedNotResolvedSessionName - errors', () => {
      it('should have a alterTerminatedNotResolvedSessionName function', (done) => {
        try {
          assert.equal(true, typeof a.alterTerminatedNotResolvedSessionName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.alterTerminatedNotResolvedSessionName(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-alterTerminatedNotResolvedSessionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#operationOnSession - errors', () => {
      it('should have a operationOnSession function', (done) => {
        try {
          assert.equal(true, typeof a.operationOnSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.operationOnSession(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-operationOnSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alterTerminatedNotResolvedSessionStream - errors', () => {
      it('should have a alterTerminatedNotResolvedSessionStream function', (done) => {
        try {
          assert.equal(true, typeof a.alterTerminatedNotResolvedSessionStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.alterTerminatedNotResolvedSessionStream(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-alterTerminatedNotResolvedSessionStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEchoSession - errors', () => {
      it('should have a createEchoSession function', (done) => {
        try {
          assert.equal(true, typeof a.createEchoSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEchoSessionInformation - errors', () => {
      it('should have a getEchoSessionInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getEchoSessionInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getEchoSessionInformation(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getEchoSessionInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEthOamSession - errors', () => {
      it('should have a createEthOamSession function', (done) => {
        try {
          assert.equal(true, typeof a.createEthOamSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEthOamSessionInformation - errors', () => {
      it('should have a getEthOamSessionInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getEthOamSessionInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getEthOamSessionInformation(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getEthOamSessionInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTWAMPSession - errors', () => {
      it('should have a createTWAMPSession function', (done) => {
        try {
          assert.equal(true, typeof a.createTWAMPSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTWAMPSessionInformation - errors', () => {
      it('should have a getTWAMPSessionInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getTWAMPSessionInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTWAMPSessionInformation(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getTWAMPSessionInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSLA - errors', () => {
      it('should have a createSLA function', (done) => {
        try {
          assert.equal(true, typeof a.createSLA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAs - errors', () => {
      it('should have a getSLAs function', (done) => {
        try {
          assert.equal(true, typeof a.getSLAs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSLA - errors', () => {
      it('should have a deleteSLA function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSLA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteSLA(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteSLA', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSessionReferenceToSLA - errors', () => {
      it('should have a putSessionReferenceToSLA function', (done) => {
        try {
          assert.equal(true, typeof a.putSessionReferenceToSLA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putSessionReferenceToSLA(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-putSessionReferenceToSLA', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionReferencesForSLA - errors', () => {
      it('should have a getSessionReferencesForSLA function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionReferencesForSLA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSessionReferencesForSLA(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getSessionReferencesForSLA', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSessionReferenceFromSLA - errors', () => {
      it('should have a deleteSessionReferenceFromSLA function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSessionReferenceFromSLA === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteSessionReferenceFromSLA(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteSessionReferenceFromSLA', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ref', (done) => {
        try {
          a.deleteSessionReferenceFromSLA('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ref is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteSessionReferenceFromSLA', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemVersion - errors', () => {
      it('should have a getSystemVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiOrchestrateV3Agents - errors', () => {
      it('should have a getApiOrchestrateV3Agents function', (done) => {
        try {
          assert.equal(true, typeof a.getApiOrchestrateV3Agents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiOrchestrateV3AgentsAgentId - errors', () => {
      it('should have a getApiOrchestrateV3AgentsAgentId function', (done) => {
        try {
          assert.equal(true, typeof a.getApiOrchestrateV3AgentsAgentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.getApiOrchestrateV3AgentsAgentId(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getApiOrchestrateV3AgentsAgentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiOrchestrateV3AgentsAgentId - errors', () => {
      it('should have a deleteApiOrchestrateV3AgentsAgentId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiOrchestrateV3AgentsAgentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.deleteApiOrchestrateV3AgentsAgentId(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteApiOrchestrateV3AgentsAgentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiOrchestrateV3AgentsConfiguration - errors', () => {
      it('should have a postApiOrchestrateV3AgentsConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.postApiOrchestrateV3AgentsConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postApiOrchestrateV3AgentsConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-postApiOrchestrateV3AgentsConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiOrchestrateV3AgentsConfiguration - errors', () => {
      it('should have a getApiOrchestrateV3AgentsConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getApiOrchestrateV3AgentsConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiOrchestrateV3AgentsConfigurationAgentId - errors', () => {
      it('should have a getApiOrchestrateV3AgentsConfigurationAgentId function', (done) => {
        try {
          assert.equal(true, typeof a.getApiOrchestrateV3AgentsConfigurationAgentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.getApiOrchestrateV3AgentsConfigurationAgentId(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getApiOrchestrateV3AgentsConfigurationAgentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putApiOrchestrateV3AgentsConfigurationAgentId - errors', () => {
      it('should have a putApiOrchestrateV3AgentsConfigurationAgentId function', (done) => {
        try {
          assert.equal(true, typeof a.putApiOrchestrateV3AgentsConfigurationAgentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.putApiOrchestrateV3AgentsConfigurationAgentId(null, null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-putApiOrchestrateV3AgentsConfigurationAgentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiOrchestrateV3AgentsConfigurationAgentId - errors', () => {
      it('should have a deleteApiOrchestrateV3AgentsConfigurationAgentId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiOrchestrateV3AgentsConfigurationAgentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.deleteApiOrchestrateV3AgentsConfigurationAgentId(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteApiOrchestrateV3AgentsConfigurationAgentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiOrchestrateV3AgentsAgentIdCommand - errors', () => {
      it('should have a postApiOrchestrateV3AgentsAgentIdCommand function', (done) => {
        try {
          assert.equal(true, typeof a.postApiOrchestrateV3AgentsAgentIdCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.postApiOrchestrateV3AgentsAgentIdCommand(null, null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-postApiOrchestrateV3AgentsAgentIdCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiOrchestrateV3AgentsSessions - errors', () => {
      it('should have a getApiOrchestrateV3AgentsSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getApiOrchestrateV3AgentsSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiOrchestrateV3AgentsSessions - errors', () => {
      it('should have a postApiOrchestrateV3AgentsSessions function', (done) => {
        try {
          assert.equal(true, typeof a.postApiOrchestrateV3AgentsSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postApiOrchestrateV3AgentsSessions('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-postApiOrchestrateV3AgentsSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiOrchestrateV3AgentsSession - errors', () => {
      it('should have a postApiOrchestrateV3AgentsSession function', (done) => {
        try {
          assert.equal(true, typeof a.postApiOrchestrateV3AgentsSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postApiOrchestrateV3AgentsSession(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-postApiOrchestrateV3AgentsSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putApiOrchestrateV3AgentsSession - errors', () => {
      it('should have a putApiOrchestrateV3AgentsSession function', (done) => {
        try {
          assert.equal(true, typeof a.putApiOrchestrateV3AgentsSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiOrchestrateV3AgentsSessionSessionId - errors', () => {
      it('should have a getApiOrchestrateV3AgentsSessionSessionId function', (done) => {
        try {
          assert.equal(true, typeof a.getApiOrchestrateV3AgentsSessionSessionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.getApiOrchestrateV3AgentsSessionSessionId(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getApiOrchestrateV3AgentsSessionSessionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiOrchestrateV3AgentsSessionSessionId - errors', () => {
      it('should have a deleteApiOrchestrateV3AgentsSessionSessionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiOrchestrateV3AgentsSessionSessionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.deleteApiOrchestrateV3AgentsSessionSessionId(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteApiOrchestrateV3AgentsSessionSessionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiOrchestrateV3AgentsSessionsAgentId - errors', () => {
      it('should have a getApiOrchestrateV3AgentsSessionsAgentId function', (done) => {
        try {
          assert.equal(true, typeof a.getApiOrchestrateV3AgentsSessionsAgentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.getApiOrchestrateV3AgentsSessionsAgentId(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getApiOrchestrateV3AgentsSessionsAgentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiOrchestrateV3AgentsSessionsAgentId - errors', () => {
      it('should have a deleteApiOrchestrateV3AgentsSessionsAgentId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiOrchestrateV3AgentsSessionsAgentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.deleteApiOrchestrateV3AgentsSessionsAgentId(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteApiOrchestrateV3AgentsSessionsAgentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiOrchestrateV3AgentsSessionstatusSessionId - errors', () => {
      it('should have a getApiOrchestrateV3AgentsSessionstatusSessionId function', (done) => {
        try {
          assert.equal(true, typeof a.getApiOrchestrateV3AgentsSessionstatusSessionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.getApiOrchestrateV3AgentsSessionstatusSessionId(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getApiOrchestrateV3AgentsSessionstatusSessionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiOrchestrateV3AgentsAgentIdSecrets - errors', () => {
      it('should have a postApiOrchestrateV3AgentsAgentIdSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.postApiOrchestrateV3AgentsAgentIdSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.postApiOrchestrateV3AgentsAgentIdSecrets(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-postApiOrchestrateV3AgentsAgentIdSecrets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#openIDConfiguration - errors', () => {
      it('should have a openIDConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.openIDConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientName', (done) => {
        try {
          a.openIDConfiguration(null, (data, error) => {
            try {
              const displayE = 'clientName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#openIDToken - errors', () => {
      it('should have a openIDToken function', (done) => {
        try {
          assert.equal(true, typeof a.openIDToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientName', (done) => {
        try {
          a.openIDToken(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing grantType', (done) => {
        try {
          a.openIDToken('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'grantType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing code', (done) => {
        try {
          a.openIDToken('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'code is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.openIDToken('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientSecret', (done) => {
        try {
          a.openIDToken('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientSecret is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#openIDAuth - errors', () => {
      it('should have a openIDAuth function', (done) => {
        try {
          assert.equal(true, typeof a.openIDAuth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.openIDAuth(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing redirectUri', (done) => {
        try {
          a.openIDAuth('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'redirectUri is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing responseType', (done) => {
        try {
          a.openIDAuth('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'responseType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.openIDAuth('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scope', (done) => {
        try {
          a.openIDAuth('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'scope is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-openIDAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#samlRedirect - errors', () => {
      it('should have a samlRedirect function', (done) => {
        try {
          assert.equal(true, typeof a.samlRedirect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientName', (done) => {
        try {
          a.samlRedirect(null, (data, error) => {
            try {
              const displayE = 'clientName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-samlRedirect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#samlCallback - errors', () => {
      it('should have a samlCallback function', (done) => {
        try {
          assert.equal(true, typeof a.samlCallback === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sAMLResponse', (done) => {
        try {
          a.samlCallback(null, null, (data, error) => {
            try {
              const displayE = 'sAMLResponse is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-samlCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relayState', (done) => {
        try {
          a.samlCallback('fakeparam', null, (data, error) => {
            try {
              const displayE = 'relayState is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-samlCallback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#samlSpMetadata - errors', () => {
      it('should have a samlSpMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.samlSpMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientName', (done) => {
        try {
          a.samlSpMetadata(null, (data, error) => {
            try {
              const displayE = 'clientName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-samlSpMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGrantingTicket - errors', () => {
      it('should have a createGrantingTicket function', (done) => {
        try {
          assert.equal(true, typeof a.createGrantingTicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.createGrantingTicket(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createGrantingTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.createGrantingTicket('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createGrantingTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing service', (done) => {
        try {
          a.createGrantingTicket('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'service is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createGrantingTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createServiceTicket - errors', () => {
      it('should have a createServiceTicket function', (done) => {
        try {
          assert.equal(true, typeof a.createServiceTicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing grantingTicket', (done) => {
        try {
          a.createServiceTicket(null, null, (data, error) => {
            try {
              const displayE = 'grantingTicket is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createServiceTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing service', (done) => {
        try {
          a.createServiceTicket('fakeparam', null, (data, error) => {
            try {
              const displayE = 'service is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createServiceTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateServiceTicket - errors', () => {
      it('should have a validateServiceTicket function', (done) => {
        try {
          assert.equal(true, typeof a.validateServiceTicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ticket', (done) => {
        try {
          a.validateServiceTicket(null, null, null, (data, error) => {
            try {
              const displayE = 'ticket is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-validateServiceTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing service', (done) => {
        try {
          a.validateServiceTicket('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'service is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-validateServiceTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.validateServiceTicket('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-validateServiceTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUser - errors', () => {
      it('should have a createUser function', (done) => {
        try {
          assert.equal(true, typeof a.createUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsers - errors', () => {
      it('should have a getAllUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUser - errors', () => {
      it('should have a updateUser function', (done) => {
        try {
          assert.equal(true, typeof a.updateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.updateUser(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUserProperties - errors', () => {
      it('should have a updateUserProperties function', (done) => {
        try {
          assert.equal(true, typeof a.updateUserProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.updateUserProperties(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateUserProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUserProperties('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateUserProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should have a getUser function', (done) => {
        try {
          assert.equal(true, typeof a.getUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getUser(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should have a deleteUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteUser(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetPassword - errors', () => {
      it('should have a resetPassword function', (done) => {
        try {
          assert.equal(true, typeof a.resetPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.resetPassword(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-resetPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeEmailAddress - errors', () => {
      it('should have a changeEmailAddress function', (done) => {
        try {
          assert.equal(true, typeof a.changeEmailAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.changeEmailAddress(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-changeEmailAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeEmailAddress('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-changeEmailAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAuthenticatedUserProperties - errors', () => {
      it('should have a updateAuthenticatedUserProperties function', (done) => {
        try {
          assert.equal(true, typeof a.updateAuthenticatedUserProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAuthenticatedUserProperties(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateAuthenticatedUserProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticatedUser - errors', () => {
      it('should have a getAuthenticatedUser function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticatedUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthenticatedUser - errors', () => {
      it('should have a deleteAuthenticatedUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthenticatedUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAuthenticatedUserPassword - errors', () => {
      it('should have a updateAuthenticatedUserPassword function', (done) => {
        try {
          assert.equal(true, typeof a.updateAuthenticatedUserPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAuthenticatedUserPassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateAuthenticatedUserPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeAuthenticatedUserEmailAddress - errors', () => {
      it('should have a changeAuthenticatedUserEmailAddress function', (done) => {
        try {
          assert.equal(true, typeof a.changeAuthenticatedUserEmailAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeAuthenticatedUserEmailAddress(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-changeAuthenticatedUserEmailAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserByToken - errors', () => {
      it('should have a getUserByToken function', (done) => {
        try {
          assert.equal(true, typeof a.getUserByToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing onboardingToken', (done) => {
        try {
          a.getUserByToken(null, (data, error) => {
            try {
              const displayE = 'onboardingToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getUserByToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setUserPassword - errors', () => {
      it('should have a setUserPassword function', (done) => {
        try {
          assert.equal(true, typeof a.setUserPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setUserPassword(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-setUserPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#login - errors', () => {
      it('should have a login function', (done) => {
        try {
          assert.equal(true, typeof a.login === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logout - errors', () => {
      it('should have a logout function', (done) => {
        try {
          assert.equal(true, typeof a.logout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validate - errors', () => {
      it('should have a validate function', (done) => {
        try {
          assert.equal(true, typeof a.validate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateSessionForProxy - errors', () => {
      it('should have a validateSessionForProxy function', (done) => {
        try {
          assert.equal(true, typeof a.validateSessionForProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createService - errors', () => {
      it('should have a createService function', (done) => {
        try {
          assert.equal(true, typeof a.createService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.createService(null, null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.createService('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.createService('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantInfo - errors', () => {
      it('should have a getTenantInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setTenantIdentity - errors', () => {
      it('should have a setTenantIdentity function', (done) => {
        try {
          assert.equal(true, typeof a.setTenantIdentity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setTenantIdentity(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-setTenantIdentity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenantIdentity - errors', () => {
      it('should have a deleteTenantIdentity function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenantIdentity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subdomain', (done) => {
        try {
          a.deleteTenantIdentity(null, (data, error) => {
            try {
              const displayE = 'subdomain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteTenantIdentity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createToken - errors', () => {
      it('should have a createToken function', (done) => {
        try {
          assert.equal(true, typeof a.createToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createToken(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTokens - errors', () => {
      it('should have a getAllTokens function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTokens === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getToken - errors', () => {
      it('should have a getToken function', (done) => {
        try {
          assert.equal(true, typeof a.getToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.getToken(null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateToken - errors', () => {
      it('should have a updateToken function', (done) => {
        try {
          assert.equal(true, typeof a.updateToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.updateToken(null, null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateToken('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteToken - errors', () => {
      it('should have a deleteToken function', (done) => {
        try {
          assert.equal(true, typeof a.deleteToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.deleteToken(null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationMethod - errors', () => {
      it('should have a getAuthenticationMethod function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing emailAddress', (done) => {
        try {
          a.getAuthenticationMethod(null, (data, error) => {
            try {
              const displayE = 'emailAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getAuthenticationMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUsergroup - errors', () => {
      it('should have a createUsergroup function', (done) => {
        try {
          assert.equal(true, typeof a.createUsergroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUsergroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsergroups - errors', () => {
      it('should have a getAllUsergroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUsergroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsergroup - errors', () => {
      it('should have a getUsergroup function', (done) => {
        try {
          assert.equal(true, typeof a.getUsergroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.getUsergroup(null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUsergroup - errors', () => {
      it('should have a updateUsergroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateUsergroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.updateUsergroup(null, null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUsergroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsergroup - errors', () => {
      it('should have a deleteUsergroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsergroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.deleteUsergroup(null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRole - errors', () => {
      it('should have a createRole function', (done) => {
        try {
          assert.equal(true, typeof a.createRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRoles - errors', () => {
      it('should have a getAllRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRole - errors', () => {
      it('should have a getRole function', (done) => {
        try {
          assert.equal(true, typeof a.getRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.getRole(null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRole - errors', () => {
      it('should have a updateRole function', (done) => {
        try {
          assert.equal(true, typeof a.updateRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.updateRole(null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRole('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRole - errors', () => {
      it('should have a deleteRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.deleteRole(null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPermission - errors', () => {
      it('should have a createPermission function', (done) => {
        try {
          assert.equal(true, typeof a.createPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPermission(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPermissions - errors', () => {
      it('should have a getAllPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermission - errors', () => {
      it('should have a getPermission function', (done) => {
        try {
          assert.equal(true, typeof a.getPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permissionId', (done) => {
        try {
          a.getPermission(null, (data, error) => {
            try {
              const displayE = 'permissionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePermission - errors', () => {
      it('should have a updatePermission function', (done) => {
        try {
          assert.equal(true, typeof a.updatePermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permissionId', (done) => {
        try {
          a.updatePermission(null, null, (data, error) => {
            try {
              const displayE = 'permissionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updatePermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePermission('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updatePermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermission - errors', () => {
      it('should have a deletePermission function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permissionId', (done) => {
        try {
          a.deletePermission(null, (data, error) => {
            try {
              const displayE = 'permissionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deletePermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createScheduledExport - errors', () => {
      it('should have a createScheduledExport function', (done) => {
        try {
          assert.equal(true, typeof a.createScheduledExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createScheduledExport(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createScheduledExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExportConfigurations - errors', () => {
      it('should have a getAllExportConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.getAllExportConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExportConfiguration - errors', () => {
      it('should have a getExportConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getExportConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing confId', (done) => {
        try {
          a.getExportConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'confId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getExportConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExportConfiguration - errors', () => {
      it('should have a patchExportConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.patchExportConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing confId', (done) => {
        try {
          a.patchExportConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'confId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchExportConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExportConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchExportConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableExportConfiguration - errors', () => {
      it('should have a disableExportConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.disableExportConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing confId', (done) => {
        try {
          a.disableExportConfiguration(null, (data, error) => {
            try {
              const displayE = 'confId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-disableExportConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExportReport - errors', () => {
      it('should have a getAllExportReport function', (done) => {
        try {
          assert.equal(true, typeof a.getAllExportReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing confId', (done) => {
        try {
          a.getAllExportReport(null, (data, error) => {
            try {
              const displayE = 'confId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getAllExportReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExportReport - errors', () => {
      it('should have a getExportReport function', (done) => {
        try {
          assert.equal(true, typeof a.getExportReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing confId', (done) => {
        try {
          a.getExportReport(null, null, (data, error) => {
            try {
              const displayE = 'confId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getExportReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.getExportReport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getExportReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSolutionManifest - errors', () => {
      it('should have a getSolutionManifest function', (done) => {
        try {
          assert.equal(true, typeof a.getSolutionManifest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTimeExclusion - errors', () => {
      it('should have a createTimeExclusion function', (done) => {
        try {
          assert.equal(true, typeof a.createTimeExclusion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTimeExclusion(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createTimeExclusion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTimeExclusion - errors', () => {
      it('should have a getAllTimeExclusion function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTimeExclusion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTimeExclusion - errors', () => {
      it('should have a updateTimeExclusion function', (done) => {
        try {
          assert.equal(true, typeof a.updateTimeExclusion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTimeExclusion(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateTimeExclusion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTimeExclusion('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateTimeExclusion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeExclusion - errors', () => {
      it('should have a getTimeExclusion function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeExclusion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTimeExclusion(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getTimeExclusion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeExclusion - errors', () => {
      it('should have a deleteTimeExclusion function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTimeExclusion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteTimeExclusion(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteTimeExclusion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#timeExclusionEvents - errors', () => {
      it('should have a timeExclusionEvents function', (done) => {
        try {
          assert.equal(true, typeof a.timeExclusionEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing start', (done) => {
        try {
          a.timeExclusionEvents(null, null, null, (data, error) => {
            try {
              const displayE = 'start is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-timeExclusionEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing end', (done) => {
        try {
          a.timeExclusionEvents('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'end is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-timeExclusionEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTenantV2 - errors', () => {
      it('should have a createTenantV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createTenantV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTenantV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createTenantV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTenantsV2 - errors', () => {
      it('should have a getAllTenantsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTenantsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantV2 - errors', () => {
      it('should have a getTenantV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getTenantV2(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getTenantV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenantV2 - errors', () => {
      it('should have a deleteTenantV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenantV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.deleteTenantV2(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteTenantV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTenantV2 - errors', () => {
      it('should have a patchTenantV2 function', (done) => {
        try {
          assert.equal(true, typeof a.patchTenantV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.patchTenantV2(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchTenantV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchTenantV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchTenantV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantIdByAliasV2 - errors', () => {
      it('should have a getTenantIdByAliasV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantIdByAliasV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.getTenantIdByAliasV2(null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getTenantIdByAliasV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantSummaryByAliasV2 - errors', () => {
      it('should have a getTenantSummaryByAliasV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantSummaryByAliasV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.getTenantSummaryByAliasV2(null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getTenantSummaryByAliasV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIngestionDictionaryV2 - errors', () => {
      it('should have a getIngestionDictionaryV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getIngestionDictionaryV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getValidTypesV2 - errors', () => {
      it('should have a getValidTypesV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getValidTypesV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantMetadataV2 - errors', () => {
      it('should have a getTenantMetadataV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantMetadataV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getTenantMetadataV2(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getTenantMetadataV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTenantMetadataV2 - errors', () => {
      it('should have a updateTenantMetadataV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateTenantMetadataV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.updateTenantMetadataV2(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateTenantMetadataV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTenantMetadataV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateTenantMetadataV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsForMonitoredObject - errors', () => {
      it('should have a getAlertsForMonitoredObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsForMonitoredObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitoredObjectId', (done) => {
        try {
          a.getAlertsForMonitoredObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'monitoredObjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getAlertsForMonitoredObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.getAlertsForMonitoredObject('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getAlertsForMonitoredObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveAlertsForMonitoredObject - errors', () => {
      it('should have a getActiveAlertsForMonitoredObject function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveAlertsForMonitoredObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitoredObjectId', (done) => {
        try {
          a.getActiveAlertsForMonitoredObject(null, null, (data, error) => {
            try {
              const displayE = 'monitoredObjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getActiveAlertsForMonitoredObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActiveAlertsForMonitoredObject - errors', () => {
      it('should have a deleteActiveAlertsForMonitoredObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteActiveAlertsForMonitoredObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitoredObjectId', (done) => {
        try {
          a.deleteActiveAlertsForMonitoredObject(null, null, (data, error) => {
            try {
              const displayE = 'monitoredObjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteActiveAlertsForMonitoredObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createActiveAlertForMonitoredObject - errors', () => {
      it('should have a createActiveAlertForMonitoredObject function', (done) => {
        try {
          assert.equal(true, typeof a.createActiveAlertForMonitoredObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createActiveAlertForMonitoredObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createActiveAlertForMonitoredObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveAlertsForPolicy - errors', () => {
      it('should have a getActiveAlertsForPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveAlertsForPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getActiveAlertsForPolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getActiveAlertsForPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActiveAlertsForPolicy - errors', () => {
      it('should have a deleteActiveAlertsForPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteActiveAlertsForPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteActiveAlertsForPolicy(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteActiveAlertsForPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveAlertGroupBy - errors', () => {
      it('should have a getActiveAlertGroupBy function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveAlertGroupBy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getActiveAlertGroupBy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getActiveAlertGroupBy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveAlertsForMetaFilter - errors', () => {
      it('should have a getActiveAlertsForMetaFilter function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveAlertsForMetaFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getActiveAlertsForMetaFilter(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getActiveAlertsForMetaFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveAlertsFilteredCounts - errors', () => {
      it('should have a getActiveAlertsFilteredCounts function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveAlertsFilteredCounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getActiveAlertsFilteredCounts(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getActiveAlertsFilteredCounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateActiveAlertsSecurityState - errors', () => {
      it('should have a updateActiveAlertsSecurityState function', (done) => {
        try {
          assert.equal(true, typeof a.updateActiveAlertsSecurityState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateActiveAlertsSecurityState(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateActiveAlertsSecurityState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRequestAlertPolicy - errors', () => {
      it('should have a createRequestAlertPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createRequestAlertPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRequestAlertPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createRequestAlertPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRequestAlertPolicy - errors', () => {
      it('should have a getAllRequestAlertPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRequestAlertPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAutomaticAlertPolicy - errors', () => {
      it('should have a patchAutomaticAlertPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.patchAutomaticAlertPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAutomaticAlertPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchAutomaticAlertPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAutomaticAlertPolicy - errors', () => {
      it('should have a getAutomaticAlertPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAutomaticAlertPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRequestAlertPolicy - errors', () => {
      it('should have a getRequestAlertPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getRequestAlertPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRequestAlertPolicy(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getRequestAlertPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRequestAlertPolicy - errors', () => {
      it('should have a deleteRequestAlertPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRequestAlertPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRequestAlertPolicy(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteRequestAlertPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRequestAlertPolicy - errors', () => {
      it('should have a patchRequestAlertPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.patchRequestAlertPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchRequestAlertPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchRequestAlertPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRequestAlertPolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchRequestAlertPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertPolicyHistory - errors', () => {
      it('should have a getAlertPolicyHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertPolicyHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertingGroupByV3 - errors', () => {
      it('should have a getAlertingGroupByV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertingGroupByV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getAlertingGroupByV3(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getAlertingGroupByV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertingAggregateV3 - errors', () => {
      it('should have a getAlertingAggregateV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertingAggregateV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getAlertingAggregateV3(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getAlertingAggregateV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRamenState - errors', () => {
      it('should have a getRamenState function', (done) => {
        try {
          assert.equal(true, typeof a.getRamenState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getRamenState(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getRamenState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAnalyticsV2 - errors', () => {
      it('should have a createAnalyticsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createAnalyticsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAnalyticsV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createAnalyticsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAnalyticsV2 - errors', () => {
      it('should have a getAllAnalyticsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAnalyticsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAnalyticsV2 - errors', () => {
      it('should have a getAnalyticsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAnalyticsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing analyticsId', (done) => {
        try {
          a.getAnalyticsV2(null, (data, error) => {
            try {
              const displayE = 'analyticsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getAnalyticsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnalyticsV2 - errors', () => {
      it('should have a deleteAnalyticsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnalyticsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing analyticsId', (done) => {
        try {
          a.deleteAnalyticsV2(null, (data, error) => {
            try {
              const displayE = 'analyticsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteAnalyticsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBrandingV2 - errors', () => {
      it('should have a createBrandingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createBrandingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBrandingV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createBrandingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllBrandingsV2 - errors', () => {
      it('should have a getAllBrandingsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllBrandingsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBrandingV2 - errors', () => {
      it('should have a getBrandingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getBrandingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing brandingId', (done) => {
        try {
          a.getBrandingV2(null, (data, error) => {
            try {
              const displayE = 'brandingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getBrandingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBrandingV2 - errors', () => {
      it('should have a updateBrandingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateBrandingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing brandingId', (done) => {
        try {
          a.updateBrandingV2(null, null, (data, error) => {
            try {
              const displayE = 'brandingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateBrandingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateBrandingV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateBrandingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBrandingV2 - errors', () => {
      it('should have a deleteBrandingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBrandingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing brandingId', (done) => {
        try {
          a.deleteBrandingV2(null, (data, error) => {
            try {
              const displayE = 'brandingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteBrandingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLocaleV2 - errors', () => {
      it('should have a createLocaleV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createLocaleV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createLocaleV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createLocaleV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLocalesV2 - errors', () => {
      it('should have a getAllLocalesV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllLocalesV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocaleV2 - errors', () => {
      it('should have a getLocaleV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getLocaleV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localeId', (done) => {
        try {
          a.getLocaleV2(null, (data, error) => {
            try {
              const displayE = 'localeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getLocaleV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLocaleV2 - errors', () => {
      it('should have a updateLocaleV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateLocaleV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localeId', (done) => {
        try {
          a.updateLocaleV2(null, null, (data, error) => {
            try {
              const displayE = 'localeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateLocaleV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateLocaleV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateLocaleV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLocaleV2 - errors', () => {
      it('should have a deleteLocaleV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLocaleV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localeId', (done) => {
        try {
          a.deleteLocaleV2(null, (data, error) => {
            try {
              const displayE = 'localeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteLocaleV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCompositeMetricV2 - errors', () => {
      it('should have a createCompositeMetricV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createCompositeMetricV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCompositeMetricV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createCompositeMetricV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCompositeMetricsV2 - errors', () => {
      it('should have a getAllCompositeMetricsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllCompositeMetricsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCompositeMetricV2 - errors', () => {
      it('should have a getCompositeMetricV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getCompositeMetricV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing compositeMetricId', (done) => {
        try {
          a.getCompositeMetricV2(null, (data, error) => {
            try {
              const displayE = 'compositeMetricId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getCompositeMetricV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCompositeMetricV2 - errors', () => {
      it('should have a updateCompositeMetricV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateCompositeMetricV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing compositeMetricId', (done) => {
        try {
          a.updateCompositeMetricV2(null, null, (data, error) => {
            try {
              const displayE = 'compositeMetricId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateCompositeMetricV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCompositeMetricV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateCompositeMetricV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCompositeMetricV2 - errors', () => {
      it('should have a deleteCompositeMetricV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCompositeMetricV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing compositeMetricId', (done) => {
        try {
          a.deleteCompositeMetricV2(null, (data, error) => {
            try {
              const displayE = 'compositeMetricId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteCompositeMetricV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createConnectorConfigV2 - errors', () => {
      it('should have a createConnectorConfigV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createConnectorConfigV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createConnectorConfigV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createConnectorConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllConnectorConfigsV2 - errors', () => {
      it('should have a getAllConnectorConfigsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllConnectorConfigsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectorConfigV2 - errors', () => {
      it('should have a getConnectorConfigV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getConnectorConfigV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.getConnectorConfigV2(null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getConnectorConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateConnectorConfigV2 - errors', () => {
      it('should have a updateConnectorConfigV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateConnectorConfigV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.updateConnectorConfigV2(null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateConnectorConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateConnectorConfigV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateConnectorConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConnectorConfigV2 - errors', () => {
      it('should have a deleteConnectorConfigV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConnectorConfigV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.deleteConnectorConfigV2(null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteConnectorConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createConnectorInstanceV2 - errors', () => {
      it('should have a createConnectorInstanceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createConnectorInstanceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createConnectorInstanceV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createConnectorInstanceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllConnectorInstancesV2 - errors', () => {
      it('should have a getAllConnectorInstancesV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllConnectorInstancesV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectorInstanceV2 - errors', () => {
      it('should have a getConnectorInstanceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getConnectorInstanceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorInstanceId', (done) => {
        try {
          a.getConnectorInstanceV2(null, (data, error) => {
            try {
              const displayE = 'connectorInstanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getConnectorInstanceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateConnectorInstanceV2 - errors', () => {
      it('should have a updateConnectorInstanceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateConnectorInstanceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorInstanceId', (done) => {
        try {
          a.updateConnectorInstanceV2(null, null, (data, error) => {
            try {
              const displayE = 'connectorInstanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateConnectorInstanceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateConnectorInstanceV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateConnectorInstanceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConnectorInstanceV2 - errors', () => {
      it('should have a deleteConnectorInstanceV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConnectorInstanceV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorInstanceId', (done) => {
        try {
          a.deleteConnectorInstanceV2(null, (data, error) => {
            try {
              const displayE = 'connectorInstanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteConnectorInstanceV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDashboardV2 - errors', () => {
      it('should have a createDashboardV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createDashboardV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDashboardV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createDashboardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDashboardsV2 - errors', () => {
      it('should have a getAllDashboardsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDashboardsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDashboardV2 - errors', () => {
      it('should have a getDashboardV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getDashboardV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.getDashboardV2(null, (data, error) => {
            try {
              const displayE = 'dashboardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getDashboardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDashboardV2 - errors', () => {
      it('should have a updateDashboardV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateDashboardV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.updateDashboardV2(null, null, (data, error) => {
            try {
              const displayE = 'dashboardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateDashboardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDashboardV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateDashboardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDashboardV2 - errors', () => {
      it('should have a deleteDashboardV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDashboardV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.deleteDashboardV2(null, (data, error) => {
            try {
              const displayE = 'dashboardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteDashboardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCardV2 - errors', () => {
      it('should have a createCardV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createCardV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCardV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createCardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCardsV2 - errors', () => {
      it('should have a getAllCardsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllCardsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCardV2 - errors', () => {
      it('should have a getCardV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getCardV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cardId', (done) => {
        try {
          a.getCardV2(null, (data, error) => {
            try {
              const displayE = 'cardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getCardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCardV2 - errors', () => {
      it('should have a updateCardV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateCardV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cardId', (done) => {
        try {
          a.updateCardV2(null, null, (data, error) => {
            try {
              const displayE = 'cardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateCardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCardV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateCardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCardV2 - errors', () => {
      it('should have a deleteCardV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCardV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cardId', (done) => {
        try {
          a.deleteCardV2(null, (data, error) => {
            try {
              const displayE = 'cardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteCardV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDataCleaningProfile - errors', () => {
      it('should have a createDataCleaningProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createDataCleaningProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDataCleaningProfile(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createDataCleaningProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataCleaningProfiles - errors', () => {
      it('should have a getDataCleaningProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getDataCleaningProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataCleaningProfile - errors', () => {
      it('should have a getDataCleaningProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getDataCleaningProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.getDataCleaningProfile(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getDataCleaningProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDataCleaningProfile - errors', () => {
      it('should have a updateDataCleaningProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateDataCleaningProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.updateDataCleaningProfile(null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateDataCleaningProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDataCleaningProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateDataCleaningProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDataCleaningProfile - errors', () => {
      it('should have a deleteDataCleaningProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDataCleaningProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.deleteDataCleaningProfile(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteDataCleaningProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDataCleaningHistory - errors', () => {
      it('should have a getDataCleaningHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getDataCleaningHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitoredObjectId', (done) => {
        try {
          a.getDataCleaningHistory(null, null, (data, error) => {
            try {
              const displayE = 'monitoredObjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getDataCleaningHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.getDataCleaningHistory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getDataCleaningHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadRoadrunner - errors', () => {
      it('should have a downloadRoadrunner function', (done) => {
        try {
          assert.equal(true, typeof a.downloadRoadrunner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zone', (done) => {
        try {
          a.downloadRoadrunner(null, (data, error) => {
            try {
              const displayE = 'zone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-downloadRoadrunner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIngestionProfileV2 - errors', () => {
      it('should have a createIngestionProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createIngestionProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIngestionProfileV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createIngestionProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIngestionProfilesV2 - errors', () => {
      it('should have a getAllIngestionProfilesV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIngestionProfilesV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIngestionProfileV2 - errors', () => {
      it('should have a getIngestionProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getIngestionProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ingestionProfileId', (done) => {
        try {
          a.getIngestionProfileV2(null, (data, error) => {
            try {
              const displayE = 'ingestionProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getIngestionProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIngestionProfileV2 - errors', () => {
      it('should have a updateIngestionProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateIngestionProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ingestionProfileId', (done) => {
        try {
          a.updateIngestionProfileV2(null, null, (data, error) => {
            try {
              const displayE = 'ingestionProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateIngestionProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIngestionProfileV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateIngestionProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIngestionProfileV2 - errors', () => {
      it('should have a deleteIngestionProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIngestionProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ingestionProfileId', (done) => {
        try {
          a.deleteIngestionProfileV2(null, (data, error) => {
            try {
              const displayE = 'ingestionProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteIngestionProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMetadataConfigV2 - errors', () => {
      it('should have a createMetadataConfigV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createMetadataConfigV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMetadataConfigV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createMetadataConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllMetadataConfigsV2 - errors', () => {
      it('should have a getAllMetadataConfigsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllMetadataConfigsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataConfigV2 - errors', () => {
      it('should have a getMetadataConfigV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getMetadataConfigV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metadataConfigId', (done) => {
        try {
          a.getMetadataConfigV2(null, (data, error) => {
            try {
              const displayE = 'metadataConfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMetadataConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMetadataConfigV2 - errors', () => {
      it('should have a updateMetadataConfigV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateMetadataConfigV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metadataConfigId', (done) => {
        try {
          a.updateMetadataConfigV2(null, null, (data, error) => {
            try {
              const displayE = 'metadataConfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateMetadataConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMetadataConfigV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateMetadataConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMetadataConfigV2 - errors', () => {
      it('should have a deleteMetadataConfigV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMetadataConfigV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metadataConfigId', (done) => {
        try {
          a.deleteMetadataConfigV2(null, (data, error) => {
            try {
              const displayE = 'metadataConfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteMetadataConfigV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetricBaselineByMonitoredObjectIdV2 - errors', () => {
      it('should have a getMetricBaselineByMonitoredObjectIdV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getMetricBaselineByMonitoredObjectIdV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitoredObjectId', (done) => {
        try {
          a.getMetricBaselineByMonitoredObjectIdV2(null, null, null, null, (data, error) => {
            try {
              const displayE = 'monitoredObjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMetricBaselineByMonitoredObjectIdV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metrics', (done) => {
        try {
          a.getMetricBaselineByMonitoredObjectIdV2('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'metrics is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMetricBaselineByMonitoredObjectIdV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDuplicateMonitoredObjectReportsV2 - errors', () => {
      it('should have a getAllDuplicateMonitoredObjectReportsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDuplicateMonitoredObjectReportsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateDuplicateMonitoredObjectReportV2 - errors', () => {
      it('should have a generateDuplicateMonitoredObjectReportV2 function', (done) => {
        try {
          assert.equal(true, typeof a.generateDuplicateMonitoredObjectReportV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDuplicateMonitoredObjectReportV2 - errors', () => {
      it('should have a getDuplicateMonitoredObjectReportV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getDuplicateMonitoredObjectReportV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing duplicateMonitoredObjectResportId', (done) => {
        try {
          a.getDuplicateMonitoredObjectReportV2(null, (data, error) => {
            try {
              const displayE = 'duplicateMonitoredObjectResportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getDuplicateMonitoredObjectReportV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMonitoredObjectTagMappingV2 - errors', () => {
      it('should have a createMonitoredObjectTagMappingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createMonitoredObjectTagMappingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMonitoredObjectTagMappingV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createMonitoredObjectTagMappingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllMonitoredObjectTagMappingsV2 - errors', () => {
      it('should have a getAllMonitoredObjectTagMappingsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllMonitoredObjectTagMappingsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoredObjectTagMappingV2 - errors', () => {
      it('should have a getMonitoredObjectTagMappingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitoredObjectTagMappingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitoredObjectTagMappingId', (done) => {
        try {
          a.getMonitoredObjectTagMappingV2(null, (data, error) => {
            try {
              const displayE = 'monitoredObjectTagMappingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMonitoredObjectTagMappingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMonitoredObjectTagMappingV2 - errors', () => {
      it('should have a updateMonitoredObjectTagMappingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateMonitoredObjectTagMappingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitoredObjectTagMappingId', (done) => {
        try {
          a.updateMonitoredObjectTagMappingV2(null, null, (data, error) => {
            try {
              const displayE = 'monitoredObjectTagMappingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateMonitoredObjectTagMappingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMonitoredObjectTagMappingV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateMonitoredObjectTagMappingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMonitoredObjectTagMappingV2 - errors', () => {
      it('should have a deleteMonitoredObjectTagMappingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMonitoredObjectTagMappingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitoredObjectTagMappingId', (done) => {
        try {
          a.deleteMonitoredObjectTagMappingV2(null, (data, error) => {
            try {
              const displayE = 'monitoredObjectTagMappingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteMonitoredObjectTagMappingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllMonitoredObjectsV2 - errors', () => {
      it('should have a getAllMonitoredObjectsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllMonitoredObjectsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMonitoredObjectV2 - errors', () => {
      it('should have a createMonitoredObjectV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createMonitoredObjectV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMonitoredObjectV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createMonitoredObjectV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilteredMonitoredObjectCountV2 - errors', () => {
      it('should have a getFilteredMonitoredObjectCountV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getFilteredMonitoredObjectCountV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getFilteredMonitoredObjectCountV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getFilteredMonitoredObjectCountV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilteredMonitoredObjectListV2 - errors', () => {
      it('should have a getFilteredMonitoredObjectListV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getFilteredMonitoredObjectListV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getFilteredMonitoredObjectListV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getFilteredMonitoredObjectListV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoredObjectReportingListV2 - errors', () => {
      it('should have a getMonitoredObjectReportingListV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitoredObjectReportingListV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getMonitoredObjectReportingListV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMonitoredObjectReportingListV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoredObjectGeoClustersV2 - errors', () => {
      it('should have a getMonitoredObjectGeoClustersV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitoredObjectGeoClustersV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing precision', (done) => {
        try {
          a.getMonitoredObjectGeoClustersV2(null, null, null, (data, error) => {
            try {
              const displayE = 'precision is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMonitoredObjectGeoClustersV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.getMonitoredObjectGeoClustersV2('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMonitoredObjectGeoClustersV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoredObjectMetadataDetailsV2 - errors', () => {
      it('should have a getMonitoredObjectMetadataDetailsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitoredObjectMetadataDetailsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilteredMonitoredObjectMetadataDetailsV2 - errors', () => {
      it('should have a getFilteredMonitoredObjectMetadataDetailsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getFilteredMonitoredObjectMetadataDetailsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getFilteredMonitoredObjectMetadataDetailsV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getFilteredMonitoredObjectMetadataDetailsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoredObjectV2 - errors', () => {
      it('should have a getMonitoredObjectV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitoredObjectV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monObjId', (done) => {
        try {
          a.getMonitoredObjectV2(null, (data, error) => {
            try {
              const displayE = 'monObjId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMonitoredObjectV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMonitoredObjectV2 - errors', () => {
      it('should have a updateMonitoredObjectV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateMonitoredObjectV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monObjId', (done) => {
        try {
          a.updateMonitoredObjectV2(null, null, (data, error) => {
            try {
              const displayE = 'monObjId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateMonitoredObjectV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMonitoredObjectV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateMonitoredObjectV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMonitoredObjectV2 - errors', () => {
      it('should have a deleteMonitoredObjectV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMonitoredObjectV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monObjId', (done) => {
        try {
          a.deleteMonitoredObjectV2(null, (data, error) => {
            try {
              const displayE = 'monObjId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteMonitoredObjectV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkPatchMonitoredObjectsV2 - errors', () => {
      it('should have a bulkPatchMonitoredObjectsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.bulkPatchMonitoredObjectsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.bulkPatchMonitoredObjectsV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-bulkPatchMonitoredObjectsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkInsertMonitoredObjectsV2 - errors', () => {
      it('should have a bulkInsertMonitoredObjectsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.bulkInsertMonitoredObjectsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.bulkInsertMonitoredObjectsV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-bulkInsertMonitoredObjectsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkUpdateMonitoredObjectsV2 - errors', () => {
      it('should have a bulkUpdateMonitoredObjectsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.bulkUpdateMonitoredObjectsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.bulkUpdateMonitoredObjectsV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-bulkUpdateMonitoredObjectsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkInsertMonitoredObjectsMetaV2 - errors', () => {
      it('should have a bulkInsertMonitoredObjectsMetaV2 function', (done) => {
        try {
          assert.equal(true, typeof a.bulkInsertMonitoredObjectsMetaV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.bulkInsertMonitoredObjectsMetaV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-bulkInsertMonitoredObjectsMetaV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllMonitoredObjectSummariesV2 - errors', () => {
      it('should have a getAllMonitoredObjectSummariesV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllMonitoredObjectSummariesV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoredObjectSummaryV2 - errors', () => {
      it('should have a getMonitoredObjectSummaryV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitoredObjectSummaryV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monObjId', (done) => {
        try {
          a.getMonitoredObjectSummaryV2(null, (data, error) => {
            try {
              const displayE = 'monObjId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMonitoredObjectSummaryV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllMetadataCategoryMappingsV2 - errors', () => {
      it('should have a getAllMetadataCategoryMappingsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllMetadataCategoryMappingsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataCategoryMappingV2 - errors', () => {
      it('should have a getMetadataCategoryMappingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getMetadataCategoryMappingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mappingType', (done) => {
        try {
          a.getMetadataCategoryMappingV2(null, (data, error) => {
            try {
              const displayE = 'mappingType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getMetadataCategoryMappingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMetadataCategoryMappingV2 - errors', () => {
      it('should have a updateMetadataCategoryMappingV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateMetadataCategoryMappingV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mappingType', (done) => {
        try {
          a.updateMetadataCategoryMappingV2(null, null, (data, error) => {
            try {
              const displayE = 'mappingType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateMetadataCategoryMappingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMetadataCategoryMappingV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateMetadataCategoryMappingV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSessionFilterProfileV2 - errors', () => {
      it('should have a createSessionFilterProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createSessionFilterProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSessionFilterProfileV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createSessionFilterProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSessionFilterProfilesV2 - errors', () => {
      it('should have a getAllSessionFilterProfilesV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSessionFilterProfilesV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionFilterProfileV2 - errors', () => {
      it('should have a getSessionFilterProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionFilterProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionFilterProfileId', (done) => {
        try {
          a.getSessionFilterProfileV2(null, (data, error) => {
            try {
              const displayE = 'sessionFilterProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getSessionFilterProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSessionFilterProfileV2 - errors', () => {
      it('should have a updateSessionFilterProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateSessionFilterProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionFilterProfileId', (done) => {
        try {
          a.updateSessionFilterProfileV2(null, null, (data, error) => {
            try {
              const displayE = 'sessionFilterProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateSessionFilterProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSessionFilterProfileV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateSessionFilterProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSessionFilterProfileV2 - errors', () => {
      it('should have a patchSessionFilterProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.patchSessionFilterProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionFilterProfileId', (done) => {
        try {
          a.patchSessionFilterProfileV2(null, null, (data, error) => {
            try {
              const displayE = 'sessionFilterProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchSessionFilterProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSessionFilterProfileV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchSessionFilterProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSessionFilterProfileV2 - errors', () => {
      it('should have a deleteSessionFilterProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSessionFilterProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionFilterProfileId', (done) => {
        try {
          a.deleteSessionFilterProfileV2(null, (data, error) => {
            try {
              const displayE = 'sessionFilterProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteSessionFilterProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSessionFilterV2 - errors', () => {
      it('should have a createSessionFilterV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createSessionFilterV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSessionFilterV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createSessionFilterV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSessionFiltersV2 - errors', () => {
      it('should have a getAllSessionFiltersV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSessionFiltersV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionFilterV2 - errors', () => {
      it('should have a getSessionFilterV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionFilterV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionFilterId', (done) => {
        try {
          a.getSessionFilterV2(null, (data, error) => {
            try {
              const displayE = 'sessionFilterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getSessionFilterV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSessionFilterV2 - errors', () => {
      it('should have a updateSessionFilterV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateSessionFilterV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionFilterId', (done) => {
        try {
          a.updateSessionFilterV2(null, null, (data, error) => {
            try {
              const displayE = 'sessionFilterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateSessionFilterV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSessionFilterV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateSessionFilterV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSessionFilterV2 - errors', () => {
      it('should have a patchSessionFilterV2 function', (done) => {
        try {
          assert.equal(true, typeof a.patchSessionFilterV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionFilterId', (done) => {
        try {
          a.patchSessionFilterV2(null, null, (data, error) => {
            try {
              const displayE = 'sessionFilterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchSessionFilterV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSessionFilterV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-patchSessionFilterV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSessionFilterV2 - errors', () => {
      it('should have a deleteSessionFilterV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSessionFilterV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionFilterId', (done) => {
        try {
          a.deleteSessionFilterV2(null, (data, error) => {
            try {
              const displayE = 'sessionFilterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteSessionFilterV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createThresholdProfileV2 - errors', () => {
      it('should have a createThresholdProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createThresholdProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createThresholdProfileV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createThresholdProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllThresholdProfilesV2 - errors', () => {
      it('should have a getAllThresholdProfilesV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllThresholdProfilesV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThresholdProfileV2 - errors', () => {
      it('should have a getThresholdProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getThresholdProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing thrPrfId', (done) => {
        try {
          a.getThresholdProfileV2(null, (data, error) => {
            try {
              const displayE = 'thrPrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getThresholdProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateThresholdProfileV2 - errors', () => {
      it('should have a updateThresholdProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateThresholdProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing thrPrfId', (done) => {
        try {
          a.updateThresholdProfileV2(null, null, (data, error) => {
            try {
              const displayE = 'thrPrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateThresholdProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateThresholdProfileV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateThresholdProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteThresholdProfileV2 - errors', () => {
      it('should have a deleteThresholdProfileV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteThresholdProfileV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing thrPrfId', (done) => {
        try {
          a.deleteThresholdProfileV2(null, (data, error) => {
            try {
              const displayE = 'thrPrfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteThresholdProfileV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkAssetV2 - errors', () => {
      it('should have a createNetworkAssetV2 function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkAssetV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkAssetV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createNetworkAssetV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNetworkAssetsV2 - errors', () => {
      it('should have a getAllNetworkAssetsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllNetworkAssetsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAssetV2 - errors', () => {
      it('should have a getNetworkAssetV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkAssetV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAssetId', (done) => {
        try {
          a.getNetworkAssetV2(null, (data, error) => {
            try {
              const displayE = 'networkAssetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getNetworkAssetV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkAssetV2 - errors', () => {
      it('should have a updateNetworkAssetV2 function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkAssetV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAssetId', (done) => {
        try {
          a.updateNetworkAssetV2(null, null, (data, error) => {
            try {
              const displayE = 'networkAssetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateNetworkAssetV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNetworkAssetV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateNetworkAssetV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkAssetV2 - errors', () => {
      it('should have a deleteNetworkAssetV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkAssetV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAssetId', (done) => {
        try {
          a.deleteNetworkAssetV2(null, (data, error) => {
            try {
              const displayE = 'networkAssetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteNetworkAssetV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilteredNetworkAssetCountV2 - errors', () => {
      it('should have a getFilteredNetworkAssetCountV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getFilteredNetworkAssetCountV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getFilteredNetworkAssetCountV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getFilteredNetworkAssetCountV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilteredNetworkAssetListV2 - errors', () => {
      it('should have a getFilteredNetworkAssetListV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getFilteredNetworkAssetListV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getFilteredNetworkAssetListV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getFilteredNetworkAssetListV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAssetGroupByV2 - errors', () => {
      it('should have a getNetworkAssetGroupByV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkAssetGroupByV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getNetworkAssetGroupByV2(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getNetworkAssetGroupByV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bootstrapsthemonitoredobjectcacheforalltenants - errors', () => {
      it('should have a bootstrapsthemonitoredobjectcacheforalltenants function', (done) => {
        try {
          assert.equal(true, typeof a.bootstrapsthemonitoredobjectcacheforalltenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#invokesbaselinebulkupdate - errors', () => {
      it('should have a invokesbaselinebulkupdate function', (done) => {
        try {
          assert.equal(true, typeof a.invokesbaselinebulkupdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purgesanalyticsreports - errors', () => {
      it('should have a purgesanalyticsreports function', (done) => {
        try {
          assert.equal(true, typeof a.purgesanalyticsreports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bootstrapsthenetworkassetcacheforalltenants - errors', () => {
      it('should have a bootstrapsthenetworkassetcacheforalltenants function', (done) => {
        try {
          assert.equal(true, typeof a.bootstrapsthenetworkassetcacheforalltenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIngestionDictionaryV3 - errors', () => {
      it('should have a createIngestionDictionaryV3 function', (done) => {
        try {
          assert.equal(true, typeof a.createIngestionDictionaryV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIngestionDictionaryV3(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createIngestionDictionaryV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIngestionDictionariesV3 - errors', () => {
      it('should have a getAllIngestionDictionariesV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIngestionDictionariesV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIngestionDictionaryV3 - errors', () => {
      it('should have a getIngestionDictionaryV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getIngestionDictionaryV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ingestionDictionaryId', (done) => {
        try {
          a.getIngestionDictionaryV3(null, (data, error) => {
            try {
              const displayE = 'ingestionDictionaryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getIngestionDictionaryV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIngestionDictionaryV3 - errors', () => {
      it('should have a updateIngestionDictionaryV3 function', (done) => {
        try {
          assert.equal(true, typeof a.updateIngestionDictionaryV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ingestionDictionaryId', (done) => {
        try {
          a.updateIngestionDictionaryV3(null, null, (data, error) => {
            try {
              const displayE = 'ingestionDictionaryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateIngestionDictionaryV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIngestionDictionaryV3('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateIngestionDictionaryV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIngestionDictionaryV3 - errors', () => {
      it('should have a deleteIngestionDictionaryV3 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIngestionDictionaryV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ingestionDictionaryId', (done) => {
        try {
          a.deleteIngestionDictionaryV3(null, (data, error) => {
            try {
              const displayE = 'ingestionDictionaryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteIngestionDictionaryV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDatasourceReconciliationTasksV3 - errors', () => {
      it('should have a createDatasourceReconciliationTasksV3 function', (done) => {
        try {
          assert.equal(true, typeof a.createDatasourceReconciliationTasksV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing datasource', (done) => {
        try {
          a.createDatasourceReconciliationTasksV3(null, null, null, (data, error) => {
            try {
              const displayE = 'datasource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createDatasourceReconciliationTasksV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDatasourceReconciliationTasksV3('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createDatasourceReconciliationTasksV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDatasourceReindexTasksV3 - errors', () => {
      it('should have a createDatasourceReindexTasksV3 function', (done) => {
        try {
          assert.equal(true, typeof a.createDatasourceReindexTasksV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing datasource', (done) => {
        try {
          a.createDatasourceReindexTasksV3(null, null, null, null, (data, error) => {
            try {
              const displayE = 'datasource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createDatasourceReindexTasksV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDatasourceReindexTasksV3('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createDatasourceReindexTasksV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupByV3 - errors', () => {
      it('should have a getGroupByV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getGroupByV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getGroupByV3(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getGroupByV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAggregateV3 - errors', () => {
      it('should have a getAggregateV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getAggregateV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getAggregateV3(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getAggregateV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTcpThroughputTestReportV3 - errors', () => {
      it('should have a createTcpThroughputTestReportV3 function', (done) => {
        try {
          assert.equal(true, typeof a.createTcpThroughputTestReportV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTcpThroughputTestReportV3(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-createTcpThroughputTestReportV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTcpThroughputTestReportsV3 - errors', () => {
      it('should have a getAllTcpThroughputTestReportsV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTcpThroughputTestReportsV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTcpThroughputReportsForTestV3 - errors', () => {
      it('should have a getAllTcpThroughputReportsForTestV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTcpThroughputReportsForTestV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testId', (done) => {
        try {
          a.getAllTcpThroughputReportsForTestV3(null, null, null, (data, error) => {
            try {
              const displayE = 'testId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getAllTcpThroughputReportsForTestV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTcpThroughputTestReportV3 - errors', () => {
      it('should have a getTcpThroughputTestReportV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getTcpThroughputTestReportV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testId', (done) => {
        try {
          a.getTcpThroughputTestReportV3(null, null, null, (data, error) => {
            try {
              const displayE = 'testId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getTcpThroughputTestReportV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runId', (done) => {
        try {
          a.getTcpThroughputTestReportV3('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'runId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getTcpThroughputTestReportV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTcpThroughputTestReportV3 - errors', () => {
      it('should have a updateTcpThroughputTestReportV3 function', (done) => {
        try {
          assert.equal(true, typeof a.updateTcpThroughputTestReportV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testId', (done) => {
        try {
          a.updateTcpThroughputTestReportV3(null, null, null, (data, error) => {
            try {
              const displayE = 'testId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateTcpThroughputTestReportV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runId', (done) => {
        try {
          a.updateTcpThroughputTestReportV3('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'runId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateTcpThroughputTestReportV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTcpThroughputTestReportV3('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-updateTcpThroughputTestReportV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTcpThroughputTestReportV3 - errors', () => {
      it('should have a deleteTcpThroughputTestReportV3 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTcpThroughputTestReportV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testId', (done) => {
        try {
          a.deleteTcpThroughputTestReportV3(null, null, (data, error) => {
            try {
              const displayE = 'testId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteTcpThroughputTestReportV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runId', (done) => {
        try {
          a.deleteTcpThroughputTestReportV3('fakeparam', null, (data, error) => {
            try {
              const displayE = 'runId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-deleteTcpThroughputTestReportV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilteredTcpThroughputCountV3 - errors', () => {
      it('should have a getFilteredTcpThroughputCountV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getFilteredTcpThroughputCountV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getFilteredTcpThroughputCountV3(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-accedian_skylight-adapter-getFilteredTcpThroughputCountV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilteredTcpThroughputListV3 - errors', () => {
      it('should have a getFilteredTcpThroughputListV3 function', (done) => {
        try {
          assert.equal(true, typeof a.getFilteredTcpThroughputListV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
